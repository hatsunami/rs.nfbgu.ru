<?php

namespace common\components\forms;

use common\models\db\User;
use Yii;
use yii\base\Model;

class Login extends Model
{
    public $login;
    public $password;

    public $user;
    private $class;

    public function __construct(User $class, array $config = [])
    {
        $this->class = $class;
        parent::__construct($config);
    }


    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            [['login'], 'email'],
            [['password'], 'string'],
        ];
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        if (parent::validate($attributeNames, $clearErrors)) {
            $user = ($this->class)::find()->where([
                'like','email', $this->login, false
            ])->one();
            if(empty($user)) {
                $this->addError('login', Yii::t('error', 'Неверный логин или пароль'));
            } else {
                if(!Yii::$app->security->validatePassword($this->password, $user->password)){
                    $this->addError('login', Yii::t('error', 'Неверный логин или пароль'));
                } elseif ($user->state == STATE_INACTIVE) {
                    $this->addError('login', Yii::t('error', 'Пользователь находится на модерации'));
                } else {
                    $this->user = $user;
                }
            }
        }
        return !$this->hasErrors();
    }

    public function login() {
    }

    public function attributeLabels()
    {
        return [
            'login' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Пароль'),
        ];
    }
}