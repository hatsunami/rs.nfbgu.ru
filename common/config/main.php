<?php

use yii\helpers\ArrayHelper;

$config = [
    'id' => 'YiiClearProject',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'runtimePath' => '@common/runtime',
    'aliases' => [
        '@vendor' => APP_DIR . '/vendor',
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => '@common/assets/node_modules/jquery/',
                    'js' => [
                        'dist/jquery.min.js'
                    ]
                ]
            ],
        ],

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
//            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'htmlLayout' => '@backend/themes/insignia/mail/layout',
            'viewPath' => '@backend/themes/insignia/mail/views',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => [
            'class' => 'yii\db\Connection',
        ],

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false
        ],
        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource'
                ],
                'error' => [
                    'class' => 'yii\i18n\PhpMessageSource'
                ],
                'console' => [
                    'class' => 'yii\i18n\PhpMessageSource'
                ],
                'mail' => [
                    'class' => 'yii\i18n\PhpMessageSource'
                ],
            ]
        ]
    ],
    'params' => [
        'adminEmail' => 'admin@example.com',
    ]
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [
            'model' => [
                'class' => 'yii\gii\generators\model\Generator',
                'templates' => [
                    'manual' => '@backend/components/gii/generators/model/manual',
                ]
            ],
        ],
    ];
}

return ArrayHelper::merge(
    $config,
    is_file(COMMON_DIR . "/config/main.local.php") ? require COMMON_DIR . "/config/main.local.php" : []
);
