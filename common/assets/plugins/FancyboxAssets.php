<?php
namespace common\assets\plugins;

use yii\web\AssetBundle;

class FancyboxAssets extends AssetBundle
{
    public $sourcePath = '@common/assets/node_modules/';

    public function __construct(array $config = [])
    {
        $this->sourcePath .= "@fancyapps/fancybox/";
        parent::__construct($config);
    }

    public $css = [
        "dist/jquery.fancybox.min.css"
    ];
    public $js = [
        "dist/jquery.fancybox.min.js"
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}