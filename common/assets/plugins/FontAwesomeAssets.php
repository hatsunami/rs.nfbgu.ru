<?php
namespace common\assets\plugins;

use yii\web\AssetBundle;

class FontAwesomeAssets extends AssetBundle
{
    public $sourcePath = '@common/assets/node_modules/font-awesome/';

    public $css = [
        'css/font-awesome.min.css'
    ];
    public $js = [
    ];
    public $depends = [

    ];
}