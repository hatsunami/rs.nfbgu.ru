<?php

namespace common\models\data\item\searches;

use common\models\db\Author;
use common\models\db\ItemAuthor;
use common\models\db\Source;
use common\models\db\Type;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\db\Item;
use yii\helpers\ArrayHelper;

/**
 * ItemSearch represents the model behind the search form of `common\models\db\Item`.
 */
class ItemSearch extends Item
{

    public $query;

    public $startVolume;
    public $endVolume;

    public $startYear;
    public $endYear;

    public $startDate;
    public $endDate;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [["id", "type", "source_id", "half_year", "first_page", "last_page", "startYear", "endYear"], "integer"],
            [["startVolume", "endVolume"], "double"],
            [["query"], "safe"],
            [["startDate", "endDate"], "match", "pattern" => "/^\d{2}\.\d{2}\.\d{4}$/i"],
        ];
    }

    public function fields()
    {
        return [
            'id',
            'title'
        ];
    }

    public function extraFields()
    {
        return [
            'type' => function($model) {
                return $model->baseType->name;
            },
            'source' => function($model) {
                return $model->baseSource->name;
            },
            'first_page',
            'last_page',
            'volume',
            'year',
            'half_year' => function($model) {
                return $model->halfYearName;
            }
        ];
    }

    public function exportFields()
    {
        $f = ArrayHelper::merge(
            $this->fields(),
            $this->extraFields()
        );
        unset($f[0]);
        return $f;
    }

    public function attributeLabels()
    {
        return array_merge([
            'source' => Yii::t('app', 'Источник'),
            'startVolume' => Yii::t('app', 'Объем от'),
            'endVolume' => Yii::t('app', 'Объем до'),
            'startYear' => Yii::t('app', 'Год от'),
            'endYear' => Yii::t('app', 'Год до'),
            'startDate' => Yii::t('app', 'Добавлена от'),
            'endDate' => Yii::t('app', 'Добавлена до'),
        ],
        parent::attributeLabels());
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $formName = null)
    {
        $query = self::find()
            ->leftJoin(Type::tableName(), Type::tableName() . ".id = " . self::tableName() . ".type")
            ->leftJoin(Source::tableName(), Source::tableName() . ".id = " . self::tableName() . ".source_id")
            ->leftJoin(
                ItemAuthor::tableName(), ItemAuthor::tableName() . ".item = " . self::tableName().".id"
            )
            ->innerJoin(
                Author::tableName(), Author::tableName().".id = " . ItemAuthor::tableName().".author"
            )
            ->groupBy(
                self::tableName() . ".id"
            );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC
                ],
                'attributes' => [
                    'title' => [
                        'asc' => [
                            self::tableName() . ".title" => SORT_ASC
                        ],
                        'desc' => [
                            self::tableName() . ".title" => SORT_DESC
                        ],
                    ],
                    'type' => [
                        'asc' => [
                            Type::tableName() . ".name" => SORT_ASC
                        ],
                        'desc' => [
                            Type::tableName() . ".name" => SORT_DESC
                        ],
                    ],
                    'source' => [
                        'asc' => [
                            Source::tableName() . ".name" => SORT_ASC
                        ],
                        'desc' => [
                            Source::tableName() . ".name" => SORT_DESC
                        ],
                    ],
                    'pages' => [
                        'asc' => [
                            self::tableName() . ".first_page" => SORT_ASC,
                            self::tableName() . ".last_page" => SORT_ASC
                        ],
                        'desc' => [
                            self::tableName() . ".first_page" => SORT_DESC,
                            self::tableName() . ".last_page" => SORT_DESC,
                        ],
                    ],
                    'volume',
                    'year',
                    'half_year',
                    'created_at' => [
                        'asc' => [
                            self::tableName() . ".created_at" => SORT_ASC
                        ],
                        'desc' => [
                            self::tableName() . ".created_at" => SORT_DESC
                        ],
                    ],
                ],
            ]
        ]);

        $this->load($params, $formName);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query
            ->andFilterWhere([
                self::tableName().".id" => $this->id,
                self::tableName().".type" => $this->type,
                self::tableName().".source_id" => $this->source_id,
                self::tableName().".half_year" =>$this->half_year
            ])
            ->andFilterWhere([
                'and',
                ['>=', 'volume', $this->startVolume],
                ['<=', 'volume', $this->endVolume]
            ])
            ->andFilterWhere([
                'and',
                ['>=', 'first_page', $this->first_page],
                ['<=', 'last_page', $this->last_page]
            ])
            ->andFilterWhere([
                'and',
                ['>=', 'year', $this->startYear],
                ['<=', 'year', $this->endYear]
            ])
            ->andFilterWhere([
            'OR',
            ['like', self::tableName().'.title', $this->query],
            ['like', Type::tableName().'.name', $this->query],
            ['like', Source::tableName().'.name', $this->query],
            ['like', self::tableName().'.year', $this->query],
            ['like', Author::tableName().'.name', $this->query],
        ]);
        if(!empty($this->startDate)){
            preg_match("/^(\d{2})\.(\d{2})\.(\d{4})$/i", $this->startDate, $found);
            $startTime = mktime(0, 0, 0, $found[2], $found[1], $found[3]);
            $query->andFilterWhere(
                [
                    ">=", self::tableName().".created_at", $startTime
                ]
            );
        }
        if(!empty($this->endDate)){
            preg_match("/^(\d{2})\.(\d{2})\.(\d{4})$/i", $this->endDate, $found);
            $endTime = mktime(23, 59, 59, $found[2], $found[1], $found[3]);
            $query->andFilterWhere(
                [
                    "<=", self::tableName().".created_at", $endTime
                ]
            );
        }
        return $dataProvider;
    }
}
