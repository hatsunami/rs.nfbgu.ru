<?php

namespace common\models\db;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%author}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property integer $user
 * @property integer $is_deleted
 * @property integer $created_at
 * @property integer $updated_at
 */
class Author extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function fields()
    {
        return [
            'id',
            'name',
            'type' => function($model){
                return $model->typeName;
            },
            'user'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%author}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'user'], 'required'],
            [['type', 'user', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['is_deleted'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Имя автора'),
            'type' => Yii::t('app', 'Преподаватель/Студент'),
            'is_deleted' => Yii::t('app', 'Отображение'),
            'user' => Yii::t('app', 'Пользователь'),
            'created_at' => Yii::t('app', 'Добавлен'),
            'updated_at' => Yii::t('app', 'Изменен'),
        ];
    }

    public static function typeName($type)
    {
        switch ($type)
        {
            case AUTHOR_TYPE_TEACHER:
                return Yii::t('app', 'Преподаватель');
            case AUTHOR_TYPE_STUDENT:
                return Yii::t('app', 'Студент');
        }
    }

    public function getTypeName()
    {
        return self::typeName($this->type);
    }

    public function delete()
    {
        $flag = false;
        if (Item::find()->where(['source_id' => $this->id])->count('id') != 0) {
            $flag = true;
        }
        if ($flag) {
            $this->is_deleted = IS_DELETED;
            $this->save();
        } else {
            return parent::delete();
        }
    }
}
