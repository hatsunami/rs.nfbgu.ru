<?php

namespace common\models\db;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%source}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $user
 * @property integer $is_deleted
 * @property integer $created_at
 * @property integer $updated_at
 */
class Source extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function fields()
    {
        return [
          'id',
          'name',
          'user'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%source}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'user'], 'required'],
            [['user', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['is_deleted'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'is_deleted' => Yii::t('app', 'Отображение'),
            'user' => Yii::t('app', 'Пользователь'),
            'created_at' => Yii::t('app', 'Добавлен'),
            'updated_at' => Yii::t('app', 'Изменен'),
        ];
    }

    public function delete()
    {
        $flag = false;
        if (Item::find()->where(['source_id' => $this->id])->count('id') != 0) {
            $flag = true;
        }
        if ($flag) {
            $this->is_deleted = IS_DELETED;
            $this->save();
        } else {
            return parent::delete();
        }
    }
}
