<?php

namespace common\models\db;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%user_data}}".
 *
 * @property int $id ID
 * @property string $name ФИО
 * @property int $created_at Добавлен
 * @property int $updated_at Изменен
 */
class UserData extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_data}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'ФИО'),
            'created_at' => Yii::t('app', 'Добавлен'),
            'updated_at' => Yii::t('app', 'Изменен'),
        ];
    }

    public function getBaseUser() {
        return $this->hasOne(User::class, [
            'id'=>'id'
        ]);
    }
}
