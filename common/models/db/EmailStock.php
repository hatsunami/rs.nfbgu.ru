<?php

namespace common\models\db;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%email_stock}}".
 *
 * @property integer $id
 * @property string $to
 * @property string $subject
 * @property string $message
 * @property integer $state
 * @property integer $created_at
 * @property integer $updated_at
 */
class EmailStock extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%email_stock}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['to', 'subject', 'message'], 'required'],
            [['to'], 'email'],
            [['message'], 'string'],
            [['state', 'created_at', 'updated_at'], 'integer'],
            [['to', 'subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'to' => Yii::t('app', 'Кому'),
            'subject' => Yii::t('app', 'Тема'),
            'message' => Yii::t('app', 'Текст email сообщения'),
            'state' => Yii::t('app', 'Состояние'),
            'created_at' => Yii::t('app', 'Добавлен'),
            'updated_at' => Yii::t('app', 'Изменен'),
        ];
    }

    /**
     * @param $to
     * @param $message
     * @param $text
     */
    public static function add($to, $subject, $message)
    {
        (new self([
            "to" => $to,
            "subject" => $subject,
            "message" => $message
        ]))->save();
    }

    public static function compose($template, $params = []){
        return APP()
            ->mailer
            ->render($template, $params, APP()->mailer->htmlLayout);
    }
}
