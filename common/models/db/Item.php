<?php

namespace common\models\db;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%item}}".
 *
 * @property int $id ID
 * @property string $title Название статьи
 * @property int $type Тип статьи
 * @property int $source_id ID ресурса
 * @property int $first_page Первая страница
 * @property int $last_page Последняя страница
 * @property string $volume Объем
 * @property int $year Год написания
 * @property int $user Пользователь
 * @property int $half_year Полугодие
 * @property int $created_at Добавлен
 * @property int $updated_at Изменен
 */
class Item extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    public function fields()
    {
        return [
            'id',
            'title'
        ];
    }

    public function extraFields()
    {
        return [
            'type',
            'source_id',
            'first_page',
            'last_page',
            'volume',
            'year',
            'half_year',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'type', 'source_id', 'first_page', 'last_page', 'volume', 'year', 'half_year', 'user'], 'required'],
            [['type', 'source_id', 'first_page', 'last_page', 'year', 'half_year', 'created_at', 'updated_at', 'user'], 'integer'],
            [['volume'], 'number'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    public static function halfYearName($half_year)
    {
        switch ($half_year)
        {
            case HALF_YEAR_FIRST:
                return Yii::t('app', 'Первое полугодие');
            case HALF_YEAR_SECOND:
                return Yii::t('app', 'Второе полугодие');
        }
    }

    public function getHalfYearName()
    {
        return self::halfYearName($this->half_year);
    }

    public function getBaseType()
    {
        return $this->hasOne(Type::class, [
            'id' => 'type',
        ]);
    }

    public function getBaseSource()
    {
        return $this->hasOne(Source::class, [
            'id' => 'source_id',
        ]);
    }

    public function getBaseAuthors()
    {
        return $this->hasMany(ItemAuthor::class, [
            'item' => 'id',
        ]);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Название статьи'),
            'type' => Yii::t('app', 'Тип статьи'),
            'source_id' => Yii::t('app', 'Источник'),
            'first_page' => Yii::t('app', 'Первая страница'),
            'last_page' => Yii::t('app', 'Последняя страница'),
            'volume' => Yii::t('app', 'Объем'),
            'year' => Yii::t('app', 'Год написания'),
            'user' => Yii::t('app', 'Пользователь'),
            'half_year' => Yii::t('app', 'Полугодие'),
            'created_at' => Yii::t('app', 'Добавлен'),
            'updated_at' => Yii::t('app', 'Изменен'),
        ];
    }

    public function afterDelete()
    {
        parent::afterDelete();
        $authors = ItemAuthor::find()->where([
            "item" => $this->id
        ])->all();
        if (!empty($authors)){
            foreach ($authors as $author){
                $author->delete();
            }
        }
    }
}
