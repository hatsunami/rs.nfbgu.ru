<?php

namespace common\models\db;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property int $id ID
 * @property string $email Email
 * @property string $password Пароль
 * @property int $place Учебное заведение
 * @property int $type Тип пользователя
 * @property int $state Состояние
 * @property string $hash Хэш
 * @property string $auth_key Ключ
 * @property string $token Токен
 * @property int $created_at Добавлен
 * @property int $updated_at Изменен
 */
class User extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            [['place', 'type', 'state', 'created_at', 'updated_at'], 'integer'],
            [['email'], 'string', 'max' => 100],
            [['password', 'hash', 'auth_key', 'token'], 'string', 'max' => 64],
            [['email'], 'unique'],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert){
            $user = UserData::findOne($this->id);
            if(empty($user)) {
                $user = new UserData([
                    'id' => $this->id
                ]);
                $user->save();
            }
        }
        if($insert || isset($changedAttributes['password'])){
            $password = Yii::$app->security->generatePasswordHash($this->password);
            self::updateAll([
                'password' => $password
            ], [
                'id' => $this->id
            ]);
        }
        if (empty($this->token)){
            self::updateAll([
                "token" => $this->id . "_" . APP()->security->generateRandomString(32),
            ],
                [
                   "id" => $this->id,
                ]);
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete()
    {
        UserData::findOne($this->id)->delete();
        parent::afterDelete();
    }

    public function getBaseUserData()
    {
        return $this->hasOne(UserData::class, [
            'id'=>'id'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Пароль'),
            'place' => Yii::t('app', 'Учебное заведение'),
            'type' => Yii::t('app', 'Тип пользователя'),
            'state' => Yii::t('app', 'Состояние'),
            'hash' => Yii::t('app', 'Хэш'),
            'auth_key' => Yii::t('app', 'Ключ'),
            'created_at' => Yii::t('app', 'Добавлен'),
            'updated_at' => Yii::t('app', 'Изменен'),
        ];
    }

    public static function typeName($type)
    {
        switch ($type)
        {
            case USER_TYPE_ADMIN:
                return Yii::t('app', 'Администратор');
            case USER_TYPE_MANAGER:
                return Yii::t('app', 'Менеджер');
            case USER_TYPE_CLIENT:
                return Yii::t('app', 'Пользователь');
        }
    }

    public function getTypeName()
    {
        return self::typeName($this->type);
    }

    public static function stateName($state)
    {
        switch ($state)
        {
            case STATE_INACTIVE:
                return Yii::t('app', 'В ожидании');
            case STATE_ACTIVE:
                return Yii::t('app', 'Активен');
        }
    }

    public function getStateName()
    {
        return self::stateName($this->state);
    }
}
