<?php

namespace common\models\db;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%item_author}}".
 *
 * @property integer $id
 * @property integer $author
 * @property integer $item
 * @property integer $created_at
 * @property integer $updated_at
 */
class ItemAuthor extends \yii\db\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%item_author}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author', 'item'], 'required'],
            [['author', 'item', 'created_at', 'updated_at'], 'integer'],
            [['author', 'item'], 'unique', 'targetAttribute' => ['author', 'item'], 'message' => Yii::t('error', 'Данный автор уже указан в издании')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'author' => Yii::t('app', 'Автор'),
            'item' => Yii::t('app', 'Публикация'),
            'created_at' => Yii::t('app', 'Добавлен'),
            'updated_at' => Yii::t('app', 'Изменен'),
        ];
    }

    public function getBaseAuthor(){
        return $this->hasOne(Author::class, ["id" => "author"]);
    }
}
