<?php

namespace common\models\db;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%settings}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $value
 * @property resource $encrypt
 * @property integer $use_encrypt
 * @property integer $created_at
 * @property integer $updated_at
 */
class Settings extends \yii\db\ActiveRecord
{
    private $key = '12345';
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['value', 'encrypt'], 'string'],
            [['use_encrypt', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Параметр'),
            'value' => Yii::t('app', 'Значение параметра'),
            'encrypt' => Yii::t('app', 'Зашифрованное значение'),
            'use_encrypt' => Yii::t('app', 'Использовать шифрование'),
            'created_at' => Yii::t('app', 'Добавлен'),
            'updated_at' => Yii::t('app', 'Изменен'),
        ];
    }

    public function saveEncrypt($runValidation = true, $attributeNames = null)
    {
        $this->use_encrypt = true;
        $this->encrypt = Yii::$app->security->encryptByKey($this->value, $this->key);
        $this->value = null;

        return $this->save($runValidation, $attributeNames);
    }

    public function afterFind()
    {
        $this->value = $this->use_encrypt ? Yii::$app->security->decryptByKey($this->encrypt, $this->key) : $this->value;
        parent::afterFind();
    }

    public static function getParam($key, $defaultValue = null)
    {
        $record = self::find()->where([
            'like', 'name', $key, false
        ])->one();

        return empty($record) ? $defaultValue : $record->value;
    }

    public static function findByName($name)
    {
        return self::find()->where([
            'name' => $name
        ])->one();
    }


}
