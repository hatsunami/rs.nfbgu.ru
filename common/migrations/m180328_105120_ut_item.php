<?php

use yii\db\Schema;
use yii\db\Migration;

class m180328_105120_ut_item extends Migration
{

    private $tableName = "{{%item}}";

    public function up()
    {
        try {
            $this->addColumn($this->tableName, 'user', Schema::TYPE_INTEGER);
            $this->addCommentOnColumn($this->tableName, 'user', 'Пользователь');
            $this->alterColumn($this->tableName, 'volume', Schema::TYPE_DECIMAL.'(10, 2)');
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function down()
    {
        try {
            $this->dropColumn($this->tableName, 'user');
            $this->dropColumn($this->tableName, 'volume');
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return true;
    }
}
