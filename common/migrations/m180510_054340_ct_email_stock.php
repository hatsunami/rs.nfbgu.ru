<?php

use yii\db\Migration;

class m180510_054340_ct_email_stock extends Migration
{

    private $tableName = "{{%email_stock}}";

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDb';
        }
        try {
            $this->createTable($this->tableName, [
                'id' => $this->primaryKey(11)->notNull()->comment('ID'),
                'to' => $this->string()->notNull()->comment('Кому'),
                'subject' => $this->string()->notNull()->comment('Тема'),
                'message' => $this->text()->notNull()->comment('Текст email сообщения'),
                'state' => $this->smallInteger()->defaultValue(IS_NEW)->comment('Состояние'),
                'created_at' => $this->integer()->comment('Добавлен'),
                'updated_at' => $this->integer()->comment('Изменен')
            ], $tableOptions);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function down()
    {
        $this->dropTable($this->tableName);
        return true;
    }
}
