<?php

use yii\db\Schema;
use yii\db\Migration;

class m180508_043004_ut_user extends Migration
{

    private $tableName = "{{%user}}";

    public function up()
    {
        try {
            $this->addColumn($this->tableName, "token", Schema::TYPE_STRING . "(64)");
            $this->addCommentOnColumn($this->tableName, "token", "Токен");
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function down()
    {
        try {
            $this->dropColumn($this->tableName, "token");
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return true;
    }
}
