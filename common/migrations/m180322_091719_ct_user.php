<?php

use yii\db\Migration;

class m180322_091719_ct_user extends Migration
{

    private $tableName = "{{%user}}";

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDb';
        }
        try {
            $this->createTable($this->tableName, [
                'id' => $this->primaryKey(11)->notNull()->comment('ID'),
                'email' => $this->string(100)->unique()->notNull()->comment("Email"),
                'password' => $this->string(64)->notNull()->comment("Пароль"),
                'place' => $this->integer()->comment("Учебное заведение"),
                'type' => $this->smallInteger()->defaultValue(USER_TYPE_CLIENT)->comment("Тип пользователя"),
                'state' => $this->smallInteger()->defaultValue(STATE_INACTIVE)->comment("Состояние"),
                'hash' => $this->string(64)->comment("Хэш"),
                'auth_key' => $this->string(64)->comment("Ключ"),
                'created_at' => $this->integer()->comment('Добавлен'),
                'updated_at' => $this->integer()->comment('Изменен')
            ], $tableOptions);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function down()
    {
        $this->dropTable($this->tableName);
        return true;
    }
}
