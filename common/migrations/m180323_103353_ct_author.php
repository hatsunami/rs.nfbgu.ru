<?php

use yii\db\Migration;

class m180323_103353_ct_author extends Migration
{

    private $tableName = "{{%author}}";

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDb';
        }
        try {
            $this->createTable($this->tableName, [
                'id' => $this->primaryKey(11)->notNull()->comment('ID'),
                'name' => $this->string(255)->notNull()->comment('Имя автора'),
                'type' => $this->smallInteger()->comment('Преподаватель/Студент'),
                'is_deleted' => $this->boolean()->defaultValue(IS_NOT_DELETED)->comment('Отображение'),
                'created_at' => $this->integer()->comment('Добавлен'),
                'updated_at' => $this->integer()->comment('Изменен')
            ], $tableOptions);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function down()
    {
        $this->dropTable($this->tableName);
        return true;
    }
}
