<?php

use yii\db\Schema;
use yii\db\Migration;

class m180508_091540_ut_author extends Migration
{

    private $tableName = "{{%author}}";

    public function up()
    {
        try {
            $this->addColumn($this->tableName, 'user', Schema::TYPE_INTEGER);
            $this->addCommentOnColumn($this->tableName, 'user', 'Пользователь');
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function down()
    {
        try {
            $this->dropColumn($this->tableName, 'user');
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return true;
    }
}
