<?php

use yii\db\Migration;

class m180323_103228_ct_item extends Migration
{

    private $tableName = "{{%item}}";

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDb';
        }
        try {
            $this->createTable($this->tableName, [
                'id' => $this->primaryKey(11)->notNull()->comment('ID'),
                'title' =>$this->string(255)->notNull()->comment('Название статьи'),
                'type' =>$this->smallInteger()->notNull()->comment('Тип статьи'),
                'source_id' =>$this->integer()->notNull()->comment('ID ресурса'),
                'first_page' =>$this->integer()->notNull()->comment('Первая страница'),
                'last_page' =>$this->integer()->notNull()->comment('Последняя страница'),
                'volume' =>$this->decimal()->notNull()->comment('Объем'),
                'year' =>$this->integer()->notNull()->comment('Год написания'),
                'half_year' =>$this->smallInteger()->notNull()->comment('Полугодие'),
                'created_at' => $this->integer()->comment('Добавлен'),
                'updated_at' => $this->integer()->comment('Изменен')
            ], $tableOptions);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function down()
    {
        $this->dropTable($this->tableName);
        return true;
    }
}
