<?php

use yii\db\Migration;

class m180406_100516_ct_item_author extends Migration
{

    private $tableName = "{{%item_author}}";

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDb';
        }
        try {
            $this->createTable($this->tableName, [
                'id' => $this->primaryKey(11)->notNull()->comment('ID'),
                'author' => $this->integer()->notNull()->comment('Автор'),
                'item' => $this->integer()->notNull()->comment('Публикация'),
                'created_at' => $this->integer()->comment('Добавлен'),
                'updated_at' => $this->integer()->comment('Изменен')
            ], $tableOptions);
            $this->createIndex('IX_ITEM_AUTHOR', $this->tableName, ['author', 'item'], true);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function down()
    {
        $this->dropTable($this->tableName);
        return true;
    }
}
