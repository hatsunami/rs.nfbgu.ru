<?php

use yii\db\Migration;

class m180322_112452_ct_session extends Migration
{

    private $tableName = "{{%session}}";

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDb';
        }
        try {
            $this->createTable($this->tableName, [
                'id' => $this->char(40)->notNull()->comment('ID'),
                'expire' => $this->integer(),
                'data' => $this->text(),
            ], $tableOptions);
            $this->addPrimaryKey('PK_session_id', $this->tableName, ['id']);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function down()
    {
        $this->dropTable($this->tableName);
        return true;
    }
}
