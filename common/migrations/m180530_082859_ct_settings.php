<?php

use yii\db\Migration;

class m180530_082859_ct_settings extends Migration
{

    private $tableName = "{{%settings}}";

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDb';
        }
        try {
            $this->createTable($this->tableName, [
                'id' => $this->primaryKey(11)->notNull()->comment('ID'),
                'name' => $this->string()->unique()->notNull()->comment('Параметр'),
                'value' => $this->text()->comment('Значение параметра'),
                'encrypt' => $this->binary()->comment('Зашифрованное значение'),
                'use_encrypt' => $this->boolean()->notNull()->defaultValue(STATE_INACTIVE)->comment('Использовать шифрование'),
                'created_at' => $this->integer()->comment('Добавлен'),
                'updated_at' => $this->integer()->comment('Изменен')
            ], $tableOptions);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function down()
    {
        $this->dropTable($this->tableName);
        return true;
    }
}
