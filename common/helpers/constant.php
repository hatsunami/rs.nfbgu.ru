<?php
    defined('STATE_INACTIVE') or define('STATE_INACTIVE', 0);
    defined('STATE_ACTIVE') or define('STATE_ACTIVE', 1);

    defined('USER_TYPE_ADMIN') or define ('USER_TYPE_ADMIN', 0);
    defined('USER_TYPE_MANAGER') or define ('USER_TYPE_MANAGER', 1);
    defined('USER_TYPE_CLIENT') or define ('USER_TYPE_CLIENT', 2);

    defined('SESSION_LONG') or define ('SESSION_LONG', 3600*24*30);
    defined('SESSION_SHORT') or define ('SESSION_SHORT', 3600);

    defined('AUTHOR_TYPE_TEACHER') or define('AUTHOR_TYPE_TEACHER', 0);
    defined('AUTHOR_TYPE_STUDENT') or define('AUTHOR_TYPE_STUDENT', 1);

    defined('ITEM_TYPE_VAC') or define('ITEM_TYPE_VAC', 0);
    defined('ITEM_TYPE_CONF') or define('ITEM_TYPE_CONF', 1);
    defined('ITEM_TYPE_RINC') or define('ITEM_TYPE_RINC', 2);

    defined('HALF_YEAR_FIRST') or define('HALF_YEAR_FIRST', 1);
    defined('HALF_YEAR_SECOND') or define('HALF_YEAR_SECOND', 2);

    defined('IS_NOT_DELETED') or define('IS_NOT_DELETED', 0);
    defined('IS_DELETED') or define('IS_DELETED', 1);

    defined('IS_NEW') or define('IS_NEW', 0);
    defined('IS_OK') or define('IS_OK', 100);
    defined('IS_ERROR') or define('IS_ERROR', 200);

    defined('EMAIL_ENCRYPT_NULL') or define('EMAIL_ENCRYPT_NULL', 0);
    defined('EMAIL_ENCRYPT_SSL') or define('EMAIL_ENCRYPT_SSL', 1);
    defined('EMAIL_ENCRYPT_TLS') or define('EMAIL_ENCRYPT_TLS', 2);
