<?php
function APP(){
    return Yii::$app;
}

function REQUEST(){
    return APP()->request;
}

function RESPONSE(){
    return APP()->response;
}

function DB(){
    return APP()->db;
}

function USER(){
    return APP()->user;
}

function IDENTITY(){
    return APP()->user->identity;
}