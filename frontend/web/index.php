<?php
ini_set('display_errors', 1);
// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../../basepath.php';
require COMMON_DIR . '/helpers/constant.php';

require APP_DIR . '/vendor/autoload.php';
require APP_DIR . '/vendor/yiisoft/yii2/Yii.php';
require COMMON_DIR . '/config/bootstrap.php';

$config = require FRONTEND_DIR . '/config/main.php';
$app = new yii\web\Application($config);
require COMMON_DIR . '/helpers/shortcut.php';
$app->run();
