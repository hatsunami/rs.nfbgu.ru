<?php
	use yii\helpers\ArrayHelper;
	$frontend_config = [
	    'id' => 'YiiClearProject-Frontend',
        'basePath' => dirname(__DIR__),
        'controllerNamespace' => 'frontend\controllers',
        'homeUrl' => ['page/home'],
        'components' => [

            'request' => [
                'cookieValidationKey' => '-xHyYEyFjllBtSXHfLbOcmpIktvhh3Qm',
            ],
            'urlManager' => [
                'rules' => [
                    "/shop/i-<id:\d+>" => "shop/view",
                    "/<controller:[\w|-]+>/" => "<controller>/index",
                    "/<link:[\w|-]+>" => "page/index",
                    "/contact" => "page/contact",

                    "/" => "page/home"
                ]
            ],
            'view' => [
                'theme' => [
                    'basePath' => '@frontend/themes/treepay',
                    'baseUrl' => '@frontend/themes/treepay',
                    'pathMap' => [
                        '@frontend/views' => '@frontend/themes/treepay/views',
                    ],
                ],
            ],
        ],
        'params' => [
            "google" => null
        ]
	];

	return ArrayHelper::merge(
		is_file(COMMON_DIR . "/config/main.php") ? require COMMON_DIR . "/config/main.php" : [],
        $frontend_config,
        is_file(FRONTEND_DIR . "/config/main.local.php") ? require FRONTEND_DIR . "/config/main.local.php" : []
	);