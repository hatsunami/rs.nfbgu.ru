<?php
    use yii\helpers\Html;
    use frontend\themes\treepay\assets\bundle\Frontend;

    Frontend::register($this);
    $this->beginPage();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="<?= Yii::$app->charset; ?>">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?= Html::csrfMetaTags(); ?>
    <title><?= $this->title; ?></title>
    <?php
        $this->head();
    ?>
    <script>
        window.config = {
            name: 'TreePay LTD',
            address: 'Нефтекамск, Ленина 3',
            phone: [{
                value: '79178089909',
                format: '8(917)808-99-09'
            }, {
                value: '79178089908',
                format: '8(917)808-99-08'
            }, ],
            coord: {
                lat: 56.078845,
                lng: 54.242519
            }
        };
    </script>
</head>
<body>
<?php $this->beginBody(); ?>
<header id="section-header">
    <div class="container">
        <div class="row">
            <div class="logotype">
                <a href="/">
                    <img src="http://pitomnik-rb.ru/images/logo.png" alt="">
                </a>
            </div>
            <div class="about">
                Продажа и посадка крупномерных деревьев напрямую из питомника по всей России и СНГ
            </div>
            <div class="block-phone">
                <a href="tel:+73472669545" class="number">
                    +7 (347) 266-95-45
                </a>
                <a href="#modal-feedback" class="button" data-fancybox data-src="#modal-feedback">
                    Заказать звонок
                </a>
            </div>
        </div>
    </div>
</header>
<nav id="section-navigation">
    <div class="container">
        <?= $this->render("partial/_menu"); ?>
    </div>
</nav>
<?= $content; ?>
<footer id="section-footer">
    <div class="container">
        <div class="row">
            <div class="about">
                <div class="logotype">
                    <a href="/">
                        <img src="http://pitomnik-rb.ru/images/logo.png" alt="Logotype">
                    </a>
                </div>
                <div class="social">
                    <ul>
                        <li>
                            <a href="#" title="Мы вКонтакте"><i class="fa fa-vk"></i></a>
                        </li>
                        <li>
                            <a href="#" title="Facebook"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#" title="Twitter"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#" title="Instagram"><i class="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="contacts">
                <div class="item">
                    <div class="icon">
                        <i class="fa fa-map-marker"></i>
                    </div>
                    <div class="content"> Россия, г. Уфа, п. Зинино, ул. Строительная 1Д
                    </div>
                </div>
                <div class="item">

                    <div class="icon">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="content">
                        <a href="mailto:krupnomer-rb@yandex.ru">krupnomer-rb@yandex.ru</a>
                    </div>
                </div>
                <div class="item">
                    <div class="icon">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <div class="content">
                        <ul>
                            <li>Пн-Пт: 08:00-19:00</li>
                            <li>Сб: 09:00-17:00</li>
                            <li>Вc: Выходной</li>
                            <li>Обед: 14:00-15:00</li>
                        </ul>
                    </div>
                </div>
            </div>
            <nav class="navigation">
                <div class="title">Навигация</div>
                <?= $this->render("partial/_menu"); ?>
            </nav>
            <div class="block-phone">
                <a href="tel:+73472669545" class="number">
                    +7 (347) 266-95-45
                </a>
                <a href="#modal-feedback" class="button" data-fancybox data-src="#modal-feedback">
                    Заказать звонок
                </a>
            </div>
        </div>

    </div>
    <div class="copyright">
        <div class="container">
            &copy;2018. Все права защищены
        </div>
    </div>
</footer>

<?php
    echo $this->render("partial/_callback");
?>

<?php $this->endBody(); ?>
</body>
</html>
<?php
$this->endPage();