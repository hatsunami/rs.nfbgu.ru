<?php
use yii\helpers\Html;
/**
* @var $model common\models\db\Shop
**/
?>
<div class="image">
    <?php
        $thumb = !empty($model->image) ? $model->image : "/upload/catalog/sosna.jpg";
    ?>
    <img src="<?= $thumb; ?>" alt="<?= Html::encode($model->name); ?>">
</div>
<?= Html::a($model->name, ["shop/view", "id" => $model->id], [
    "class" => "name"
]); ?>
<div class="description">
    <?= Html::encode($model->description); ?>
</div>
<div class="bottom">
    <div class="price">
        от <?= number_format($model->price, 2, '.', ''); ?> <i class="fa fa-rub"></i>
    </div>
    <a href="#" class="button">Заказать</a>
</div>