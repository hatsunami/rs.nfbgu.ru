'use strict';

const bindElement = (element, callback) => {
    if (typeof jQuery != 'undefined') {
        let elements = jQuery(element);
        if (elements.length) {
            if (typeof callback == "function") {
                callback(elements);
            }
        }
    }
}

function sendCallback(form) {
    if (typeof jQuery != 'undefined') {
        swal({
            icon: "success",
            title: "Сообщение",
            text: "Ваша заявка успешно отправлена",
        });
        parent.jQuery.fancybox.close()
    }
}

jQuery(function() {
    jQuery("[data-fancybox]").fancybox();
    bindElement('input[type="tel"]', function(phones) {
        phones.inputmask({
            "mask": "+7 (999) 999-9999"
        });
    });
    bindElement('#section-home-slider .slider', function(elements) {
        for (var i = 0; i < elements.length; i++) {
            jQuery(elements[i]).owlCarousel({
                autoplaySpeed: 2000,
                loop: true,
                dots: false,
                autoplay: true,
                items: 1
            });
        }
    });
    bindElement('#section-reviews .slider', function(elements) {
        for (var i = 0; i < elements.length; i++) {
            jQuery(elements[i]).owlCarousel({
                autoplaySpeed: 2000,
                loop: true,
                dots: true,
                autoplay: false,
                items: 1
            });
        }
    });

    bindElement("#map", function(mapElement) {
        if (typeof ymaps != 'undefined') {
            ymaps.ready(function() {
                mapElement = jQuery(mapElement[0]);
                var center = [
                        typeof window.config.coord.lat != 'undefined' ? window.config.coord.lat : 55.76,
                        typeof window.config.coord.lng != 'undefined' ? window.config.coord.lng : 37.64
                    ],
                    map = new ymaps.Map(mapElement.attr("id"), {
                        center: center,
                        zoom: 12,
                        controls: ['zoomControl']
                    }),
                    properties = {
                        balloonContentBody: '<div class="map-style">'
                    };
                if (typeof window.config.name != 'undefined') {
                    properties.balloonContentHeader = '<h3>' + window.config.name + '</h3>';
                }
                if (typeof window.config.address != 'undefined') {
                    properties.balloonContentBody += '<p>' + window.config.address + '</p>';
                }
                if (typeof window.config.phone != 'undefined') {
                    try {
                        if (window.config.phone.length) {
                            properties.balloonContentBody += '<p><b>Телефоны:</b><br/>';
                            for (var i = 0; i < window.config.phone.length; i++) {
                                var phone = window.config.phone[i];
                                properties.balloonContentBody += '<a href="tel:' + phone.value + '">' + phone.format + '</a><br/>';
                            }
                            properties.balloonContentBody += '</p>';
                        }

                    } catch (err) {

                    }
                }
                properties.balloonContentBody += '</div>';
                var
                    placemark = new ymaps.Placemark(center, properties, {
                        preset: 'islands#dotIcon',
                        iconColor: '#735184'
                    });
                map.geoObjects.add(placemark);
            });
        }
    });
    jQuery(window).on("scroll", function(e) {
        var
            top = jQuery("#section-header").outerHeight(true),
            height = jQuery('#section-navigation').outerHeight(true);
        scroll = jQuery(window).scrollTop();
        if (scroll >= top) {
            jQuery('body').addClass("fixed").css({
                "padding-top": height
            });
        } else {
            jQuery('body').removeClass("fixed").css({
                "padding-top": 0
            });
        }

    });
});