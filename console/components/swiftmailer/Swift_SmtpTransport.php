<?php
namespace console\components\swiftmailer;

use common\models\db\Settings;

class Swift_SmtpTransport extends \Swift_SmtpTransport
{
    public function __construct($host = 'localhost', $port = 25, $security = null)
    {
        $host = Settings::getParam("server", $host);
        $port = Settings::getParam("port", $port);
        $encryption = Settings::getParam("encrypt");
        switch ($encryption){
            case EMAIL_ENCRYPT_SSL:
                $security = "ssl";
                break;
            case EMAIL_ENCRYPT_TLS:
                $security = "tls";
                break;
            case EMAIL_ENCRYPT_NULL:
                $security = null;
                break;
        }
        $username = Settings::getParam("email");
        $password = Settings::getParam("password");
        parent::__construct($host, $port, $security);
        if ($username) $this->setUsername($username);
        if ($password) $this->setPassword($password);
    }
}