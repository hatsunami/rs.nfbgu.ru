<?php
namespace console\components\console;

use Yii;
use yii\helpers\Console;

class Controller extends \yii\console\Controller
{
    public function log($text = null){
        echo Yii::t('console', $text);
        echo PHP_EOL;
    }

    public function error($text = null){
        $text = Yii::t('console', $text);
        $text = $this->ansiFormat($text, Console::FG_RED);
        echo $text;
        echo PHP_EOL;
    }

    public function isRun($command, $regexp){
        $pid = getmypid();
        exec('ps axw | grep -v grep | grep ' . escapeshellarg($command), $output);
        $enabled = false;
        foreach ($output as $process_line){
            if (preg_match($regexp, $process_line)){
                if (!preg_match("/^([^\d]*)".($pid - 1).".*$/i", $process_line) && !preg_match("/^([^\d]*)".($pid).".*$/i", $process_line) && !preg_match("/^([^\d]*)".($pid + 1).".*$/i", $process_line)) {
                    $enabled = true;
                    break;
                }
            }
        }
        return $enabled;
    }
}