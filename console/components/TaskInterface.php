<?php
namespace console\components;

interface TaskInterface
{
    public static function run();
}