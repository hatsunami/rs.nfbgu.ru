<?php
/**
 * This view is used by console/controllers/MigrateController.php
 * The following variables are available in this view:
 */
/* @var $className string the new migration class name */

echo "<?php\n";
?>

use yii\db\Schema;
use yii\db\Migration;

class <?= $className ?> extends Migration
{
<?php
$tableName = $className;
$mode = null;
if (preg_match("/_(ct|ut)_(.+)$/i", $className, $find)) {
    $tableName = $find[2];
    $mode = $find[1];
}
?>

private $tableName = "{{%<?= $tableName; ?>}}";
public function up()
{
<?php
if ($mode == "ct") {
    ?>
    $tableOptions = null;
    if ($this->db->driverName === 'mysql') {
    $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDb';
    }
    try{
    $this->createTable($this->tableName, [
    'id' => $this->primaryKey(11)->notNull()->comment('ID'),
    //add field
    'created_at' => $this->integer()->comment('Добавлен'),
    'updated_at' => $this->integer()->comment('Изменен')
    ], $tableOptions);
    }catch (Exception $e){
    echo $e->getMessage(); <?= PHP_EOL; ?>
    }
    <?php
}
if ($mode == "ut") {
    ?>
    try{
    //insert code
    }catch(Exception $e){
    echo $e->getMessage();
    }
    <?php
}
?>
}

public function down()
{
<?php
if ($mode == "ct") {
    ?>
    $this->dropTable($this->tableName);
    <?php
}
if ($mode == "ut") {
    ?>
    try{
    //insert code
    }catch(Exception $e){
    echo $e->getMessage();
    }
    <?php
}
?>
return true;
}
}
