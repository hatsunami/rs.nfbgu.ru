<?php

namespace console\tasks;

use common\models\db\EmailStock;
use console\components\TaskInterface;
use yii\validators\EmailValidator;
use Yii;

class Email implements TaskInterface
{
    private static function log($text = null, $end = true){
        echo Yii::t('console', $text);
        if ($end) echo PHP_EOL;
    }

    private static function send($to, $subject, $text = null){
        try{
            $from = APP()->mailer->getTransport()->getUsername();
            $validate = new EmailValidator(["checkDNS" => true]);
            $error = null;
            if ($validate->validate($to, $error)){
                APP()
                    ->mailer
                    ->getTransport()
                    ->start();
                APP()
                    ->mailer
                    ->compose()
                    ->setFrom($from)
                    ->setTo($to)
                    ->setSubject($subject)
                    ->setHtmlBody($text)
                    ->send();
                APP()->mailer->getTransport()->stop();
                return IS_OK;
            }else{
                return IS_ERROR;
            }
        }catch (\Exception $e){
            self::log($e->getMessage());
            return IS_ERROR;
        }
    }

    public static function run()
    {
        $emails = EmailStock::find()->where([
            "state" => IS_NEW
        ])->all();
        if (!empty($emails)){
            foreach ($emails as $email){
                self::log("Отправка письма на email " . $email->to, false);
                try{
                    $email->state = self::send($email->to, $email->subject, $email->message);
                    $email->save(false);
                    self::log($email->state);
                    self::log("...", false);
                }catch (\Exception $e){
                    self::log($e->getMessage());
                }
            }
        }
    }
}