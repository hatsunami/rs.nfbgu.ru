<?php

namespace console\controllers;

use console\components\console\Controller;
use yii\helpers\FileHelper;
use Yii;

class SystemController extends Controller
{
    /**
     * Инициализация проекта
     */
    public function actionInit(){
        #1 - Создание базовых папок
        $folder = [
            FileHelper::normalizePath(Yii::getAlias('@backend/web/assets/')),
            FileHelper::normalizePath(Yii::getAlias('@frontend/web/assets/')),
        ];
        foreach ($folder as $item){
            try{
                FileHelper::createDirectory($item);
                $this->log('Директория ' . $item . ' создана');
            }catch (\Exception $e){
                $this->error($e->getMessage());
            }
        }
    }
}