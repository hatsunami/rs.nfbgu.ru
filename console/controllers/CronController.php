<?php

namespace console\controllers;

use console\components\console\Controller;
use console\tasks\Email;

class CronController extends Controller
{
    public function actionStart(){
        if ($this->isRun("cron", "/^.*yii.*cron.*$/i")) exit;

        while(true){
            Email::run();
            $this->log("Ждем 3 сек");
            sleep(3);
        }
    }
}