<?php

namespace console\controllers;

use common\models\db\User;
use yii\console\Controller;

class TestController extends Controller
{
    public function actionInsertUser(){
        $user = new User();
        $user->email = 'admin@admin.ru';
        $user->password = '12345';
        $user->type = USER_TYPE_ADMIN;
        $user->save();
    }
}