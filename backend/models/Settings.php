<?php

namespace backend\models;


use common\models\db\Settings as _Settings;
use Yii;
use yii\base\Model;

class Settings extends Model
{
    public $email;
    public $password;
    public $server;
    public $port;
    public $encrypt;

    private $encryptFields = [
        'password'
    ];

    public function prepare(){
        array_map(function($key){
            $this->$key = _Settings::getParam($key);
        }, array_keys($this->getAttributes()));
    }

    public function rules()
    {
        return [
            [['email', 'password', 'server', 'port', 'encrypt'], 'required'],
            [['port'], 'integer'],
            [['encrypt'], 'in', 'range' => [
                EMAIL_ENCRYPT_NULL, EMAIL_ENCRYPT_SSL, EMAIL_ENCRYPT_TLS
            ]]
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Пароль'),
            'server' => Yii::t('app', 'SMTP Сервер'),
            'port' => Yii::t('app', 'Порт'),
            'encrypt' => Yii::t('app', 'Тип шифрования')
        ];
    }

    public function save()
    {
        if($this->validate()){
            array_map(function($fieldName){
                $setting = _Settings::findByName($fieldName);
                if(empty($setting)){
                    $setting = new _Settings([
                        'name' => $fieldName
                    ]);
                }
                $setting->value = $this->$fieldName;
                if(in_array($fieldName, $this->encryptFields)){
                    $setting->saveEncrypt();
                } else {
                    $setting->save();
                }
                if($setting->hasErrors()){
                    $this->addErrors($setting->getFirstErrors());
                }

            }, array_keys($this->getAttributes()));
        }
        return !$this->hasErrors();
    }

    public static function encryptName($encrypt)
    {
        switch ($encrypt)
        {
            case EMAIL_ENCRYPT_NULL:
                return Yii::t("app", 'Не использовать');
            case EMAIL_ENCRYPT_SSL:
                return 'SSL';
            case EMAIL_ENCRYPT_TLS:
                return 'TLS';
        }
    }

    public function getEncryptName()
    {
        return self::encryptName($this->encrypt);
    }
}