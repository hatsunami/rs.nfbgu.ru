<?php

namespace backend\controllers;


use backend\components\web\Controller;
use Yii;

class ErrorController extends Controller
{
    public $layout = false;

    public function actionError(){
        $exception = Yii::$app->errorHandler->exception;

        if ($exception !== null) {
            $statusCode = $exception->statusCode;
            $name = $exception->getName();
            $message = $exception->getMessage();
            if (Yii::$app->request->isAjax){
                return $message;
            }


            return $this->render('index', [
                'exception' => $exception,
                'statusCode' => $statusCode,
                'name' => $name,
                'message' => $message
            ]);
        }
    }
}