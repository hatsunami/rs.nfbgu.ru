<?php

namespace backend\controllers;


use backend\components\web\Controller;
use backend\models\Settings;
use Yii;
use yii\web\NotAcceptableHttpException;

class SettingsController extends Controller
{
    public function actionIndex()
    {
        if(IDENTITY()->type != USER_TYPE_ADMIN){
            throw new NotAcceptableHttpException();
        }

        $model = new Settings();
        $model->prepare();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $success = Yii::$app->session->setFlash('saveSettings', true);
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}