<?php
	use yii\helpers\ArrayHelper;
	$backend_config = [
	    'id' => 'YiiClearProject-Backend',
        'language' => 'ru-RU',
        'basePath' => dirname(__DIR__),
        'controllerNamespace' => 'backend\controllers',
        'homeUrl' => ['/item/item'],
        'controllerMap' => [
            'export' => 'backend\components\widgets\yii2export\controllers\ExportController'
        ],
        'components' => [
            'assetManager' => [
                'forceCopy' => false,
                'appendTimestamp' => true,
            ],
            'user' => [
                'identityClass' => 'backend\models\BackendUser',
                'enableAutoLogin' => true,
                'loginUrl' => ['/user/auth/login']
            ],
            'session' => [
              'class' => 'yii\web\DbSession'
            ],
            'request' => [
                'cookieValidationKey' => '-xHyYEyFjllBtSXHfLbOcmpIktvhh3Qm',
            ],
            'urlManager' => [
                'rules' => [
                    "export/<action:[\w|-]+>" => "/export/<action>",

                    "<action:login|logout|forgot|restore|register>" => "/user/auth/<action>",
                    "<module:[\w|-]+>/<controller:[\w|-]+>/<action:[\w|-]+>/<id:\d+>" => "/<module>/<controller>/<action>",
                    "<module:[\w|-]+>/<controller:[\w|-]+>" => "/<module>/<controller>/index",
                    "<module:[\w|-]+>/<controller:[\w|-]+>/<action:[\w|-]+>" => "/<module>/<controller>/<action>",
                    "<controller:[\w|-]+>/<action:[\w|-]+>/<id:\d+>" => "/<controller>/<action>",
                    "<controller:[\w|-]+>/" => "/<controller>/index",
                    "<controller:[\w|-]+>/<action:[\w|-]+>" => "/<controller>/<action>",
                    "/" => "/user/item/index",
                ]
            ],
            'errorHandler' => [
                'errorAction' => '/error/error',
            ],
            'view' => [
                'theme' => [
                    'basePath' => '@backend/themes/insignia',
                    'baseUrl' => '@backend/themes/insignia',
                    'pathMap' => [
                        '@backend/views' => '@backend/themes/insignia/views',
                        '@backend/modules' => '@backend/themes/insignia/modules',
                    ],
                ],
            ],
        ],
        'modules' => [
            'user' => [
                'class' => 'backend\modules\user\Module'
            ],
            'item' => [
                'class' => 'backend\modules\item\Module'
            ]
        ],
	];

	return ArrayHelper::merge(
		is_file(COMMON_DIR . "/config/main.php") ? require COMMON_DIR . "/config/main.php" : [],
        $backend_config,
        is_file(BACKEND_DIR . "/config/main.local.php") ? require BACKEND_DIR . "/config/main.local.php" : []
	);