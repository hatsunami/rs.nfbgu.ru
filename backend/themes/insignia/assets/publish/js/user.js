'use strict';

const bindElement = function (element, callback) {
    if (typeof jQuery != 'undefined') {
        var elements = jQuery(element);
        if (elements.length) {
            if (typeof callback == 'function') {
                callback(elements);
            }
        }
    }
}

function setAllElement() {
    bindElement('.i-checks', function (elements) {
        for (var i = 0; i < elements.length; i++) {
            jQuery(elements[i]).iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        }
    });
    bindElement('[data-type="number"]', function (elements) {
        jQuery(elements).inputmask({
            alias: "integer",
            min: 0,
            rightAlign: false
        });
    });
    bindElement('[data-type="decimal"]', function (elements) {
        jQuery(elements).inputmask({
            alias: "decimal",
            min: 0,
            rightAlign: false
        });
    });
    bindElement('[data-type="date"]', function (elements) {
        for (var i = 0; i < elements.length; i++) {
            flatpickr(elements[i], {
                locale: "ru",
                dateFormat: "d.m.Y"
            });
        }
    });
    var ajaxSendForm = function(settings){
        swal({
            text: settings.title,
            content: "input",
            button: {
                text: "Добавить",
                closeModal: true,
            },
        })
        .then(function (name) {
            if (name) {
                var data = {};
                    data[settings.name] = name;
                jQuery.ajax({
                    url: settings.url,
                    type: 'post',
                    dataType: 'json',
                    data: data,
                    success: function(data){
                        var option = document.createElement('option');
                        option.value = data.id;
                        option.innerText = data.name;
                        var select = settings.element;
                        select.append(option);
                        select.find('option:last').prop('selected', true);
                        select.change();
                    },
                    error: function(error){
                        swal({
                            text: error.responseText,
                            icon: 'error'
                        });
                    }
                });


            }
            throw null;
        })
    }

    bindElement("#ajax-validate-form", function(form){
        form.off("submit").on("submit", function (e) {
            e.preventDefault();
            e.stopPropagation();
            jQuery.ajax({
                url: form.attr("action"),
                type: form.attr("method"),
                dataType: "json",
                data: form.serializeArray(),
                beforeSend: function(){
                    form.find("input, select, button").prop("disabled", true);
                },
                success: function(data, state, xhr){
                    if (xhr.status == 201){
                        if (form.data("target")) {
                            var option = document.createElement('option'),
                                target = form.data("target");
                            option.value = data.id;
                            option.innerText = data.name;
                            option.setAttribute("selected", "selected");
                            jQuery("[data-target='"+target+"']").append(option);
                            jQuery("[data-target='"+target+"']").change();
                        }
                        jQuery("#ajax-view").modal('hide');
                    }
                },
                error: function(err){
                    swal({
                        title: "Внимание",
                        text: err.responseText,
                        icon: "error"
                    });
                },
                complete: function(){
                    form.find("input, select, button").prop("disabled", false);
                }
            });
        })
    });

    //for source
    bindElement("[data-action='quick-add-source']", function (elements) {
        for (var i = 0; i < elements.length; i++) {
            jQuery(elements[i]).off('click').on('click', function (e) {
                e.preventDefault();
                var el = jQuery(this);
                ajaxSendForm({
                    title: el.data('title'),
                    name: el.data('field'),
                    element: jQuery("[data-target='source-list']"),
                    url: el.data('url')
                });
            });
        }
    })
    //for type
    bindElement("[data-action='quick-add-type']", function (elements) {
        for (var i = 0; i < elements.length; i++) {
            jQuery(elements[i]).off('click').on('click', function (e) {
                e.preventDefault();
                var el = jQuery(this);
                ajaxSendForm({
                    title: el.data('title'),
                    name: el.data('field'),
                    element: jQuery("[data-target='type-list']"),
                    url: el.data('url')
                });
            });
        }
    })
    //for authors
    bindElement("[data-action='quick-add-author']", function (elements) {
        for (var i = 0; i < elements.length; i++) {
            jQuery(elements[i]).off('click').on('click', function (e) {
                e.preventDefault();
                var el = jQuery(this);
                ajaxSendForm({
                    title: el.data('title'),
                    name: el.data('field'),
                    element: jQuery("[data-target='author-list']"),
                    url: el.data('url')
                });
            });
        }
    })
    bindElement("[data-action='calculate-volume']", function (elements) {
        for (var i = 0; i < elements.length; i++) {
            jQuery(elements[i]).off('change').on('change', function (e) {
                e.preventDefault();
                var first = parseInt(elements[0].value);
                var last = parseInt(elements[elements.length - 1].value);

                if (first && last) {
                    var volume = ((Math.abs(last - first) + 1) / 16).toFixed(2);
                    jQuery("[data-target='result-volume']").val(volume);
                }
            });
        }
    });
    bindElement('table thead :checkbox', function (checkboxes) {
        if (checkboxes.length) {
            for (var i = 0; i < checkboxes.length; i++) {
                jQuery(checkboxes[i]).off("ifChanged").on("ifChanged", function (e) {
                    var inCheckoxes = jQuery(this).closest('table').find('tbody :checkbox');
                    inCheckoxes && jQuery(this).prop('checked') ? inCheckoxes.iCheck('check') : inCheckoxes.iCheck('uncheck');
                });
            }
        }
    });
    bindElement('table tbody :checkbox', function (checkboxes) {
        if (checkboxes.length) {
            for (var i = 0; i < checkboxes.length; i++) {
                jQuery(checkboxes[i]).off("ifChanged").on("ifChanged", function (e) {
                    checkboxes.parent().find(':checked').length ? jQuery("[data-action='remove-button']").removeClass('hidden') : jQuery("[data-action='remove-button']").addClass('hidden');
                });
            }
        }
    });
    bindElement('select', function (selects) {
        for (var i = 0; i < selects.length; i++) {
            jQuery(selects[i]).select2({
                language: 'ru',
                minimumResultsForSearch: jQuery(selects[i]).length < 5 ? 5 : Infinity,
                width: '100%'
            });
        }
    });
}

if (typeof jQuery != 'undefined') {
    if (typeof yii != "undefined") {
        yii.confirm = function (message, okCallback, cancelCallback) {
            swal({
                title: "Внимание!",
                text: message,
                icon: "warning",
                type: 'warning',
                buttons: {
                    cancel: {
                        text: "Отмена",
                        value: "cancel",
                        visible: true,
                        className: "",
                        closeModal: true,
                    },
                    confirm: {
                        text: "OK",
                        value: "accept",
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                }
            }).then(function (value) {
                switch (value) {
                    case "cancel":
                        if (typeof cancelCallback == "function") {
                            cancelCallback();
                        }
                        break;
                    case "accept":
                        if (typeof okCallback == "function") {
                            okCallback();
                        }
                        break;
                }
            });
        };
    }
    jQuery(function () {
        setAllElement();
        bindElement("#pjax-data", function (pjx) {
            pjx.on('pjax:end', function (xhr, options) {
                setAllElement();
            });
        });
        jQuery('#ajax-view')
            .on('loaded.bs.modal', function(event) {
                if (typeof setAllElement == 'function'){
                    setAllElement();
                }
            }).
            on('hidden.bs.modal', function(e) {
                var el = e.currentTarget;
                jQuery(this).removeData('bs.modal');
                jQuery("#ajax-view").find('.modal-body').html('Загрузка данных');
            })
            .on('shown.bs.modal', function(e){
                jQuery(".load").removeClass("hidden");
                jQuery("#ajax-view").find('[autofocus]').focus();
            });
    });
}