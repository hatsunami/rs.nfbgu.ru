<?php
namespace backend\themes\insignia\assets\plugins\slimScroll;

use yii\web\AssetBundle;

class slimScroll extends AssetBundle
{
    public $sourcePath = "@backend/themes/insignia/assets/plugins/slimScroll/publish/";

    public $js = [
        'js/jquery.slimscroll.min.js'
    ];

    public $css = [
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
}