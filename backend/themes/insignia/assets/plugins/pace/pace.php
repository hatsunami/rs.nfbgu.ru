<?php
namespace backend\themes\insignia\assets\plugins\pace;

use yii\web\AssetBundle;

class pace extends AssetBundle
{
    public $sourcePath = "@backend/themes/insignia/assets/plugins/pace/publish/";

    public $js = [
        'js/pace.min.js'
    ];

    public $css = [
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
}