<?php
namespace backend\themes\insignia\assets\plugins\inputmask;

use yii\web\AssetBundle;

class Inputmask extends AssetBundle
{
    public $sourcePath = "@backend/themes/insignia/assets/node_modules/inputmask/";

    public $js = [
        'dist/min/jquery.inputmask.bundle.min.js'
    ];

    public $css = [
        //'css/inputmask.css'
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
}