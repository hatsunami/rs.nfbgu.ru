<?php
namespace backend\themes\insignia\assets\plugins\iCheck;

use yii\web\AssetBundle;

class iCheck extends AssetBundle
{
    public $sourcePath = "@backend/themes/insignia/assets/plugins/iCheck/publish/";

    public $js = [
        'js/icheck.min.js'
    ];

    public $css = [
        'css/custom.css'
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
}