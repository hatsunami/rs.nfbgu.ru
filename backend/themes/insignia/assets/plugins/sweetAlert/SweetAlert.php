<?php
namespace backend\themes\insignia\assets\plugins\sweetAlert;

use yii\web\AssetBundle;

class SweetAlert extends AssetBundle
{
    public $sourcePath = "@backend/themes/insignia/assets/node_modules/sweetalert/";

    public $js = [
        'dist/sweetalert.min.js'
    ];

    public $css = [
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
}