<?php
namespace backend\themes\insignia\assets\plugins\select2;

use yii\web\AssetBundle;

class select2 extends AssetBundle
{
    public $sourcePath = "@backend/themes/insignia/assets/plugins/select2/publish/";

    public $js = [
        'select2.full.min.js'
    ];

    public $css = [
        'select2.min.css'
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
}