<?php
namespace backend\themes\insignia\assets\plugins\metisMenu;

use yii\web\AssetBundle;

class metisMenu extends AssetBundle
{
    public $sourcePath = "@backend/themes/insignia/assets/plugins/metisMenu/publish/";

    public $js = [
        'js/jquery.metisMenu.js'
    ];

    public $css = [
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
}