<?php
namespace backend\themes\insignia\assets\plugins\flatPickr;

use yii\web\AssetBundle;

class flatPickr extends AssetBundle
{
    public $sourcePath = "@backend/themes/insignia/assets/node_modules/flatpickr/";

    public $js = [
        'dist/flatpickr.min.js',
        'dist/l10n/ru.js'
    ];

    public $css = [
        'dist/flatpickr.min.css',
        'dist/ie.css',
        'dist/themes/material_green.css'
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
}