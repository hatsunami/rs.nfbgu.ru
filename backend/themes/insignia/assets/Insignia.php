<?php
namespace backend\themes\insignia\assets;

use yii\web\AssetBundle;

class Insignia extends AssetBundle
{
    public $sourcePath = "@backend/themes/insignia/assets/publish/";

    public $js = [
        'js/inspinia.js',
        'js/user.js'
    ];

    public $css = [
        'css/animate.css',
        'css/style.css',
        'css/user.css',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
        'common\assets\plugins\FontAwesomeAssets',
        'backend\themes\insignia\assets\plugins\iCheck\iCheck',
        'backend\themes\insignia\assets\plugins\metisMenu\metisMenu',
        'backend\themes\insignia\assets\plugins\slimScroll\slimScroll',
        'backend\themes\insignia\assets\plugins\pace\pace',
        'backend\themes\insignia\assets\plugins\sweetAlert\SweetAlert',
        'backend\themes\insignia\assets\plugins\select2\select2',
        'backend\themes\insignia\assets\plugins\flatPickr\flatPickr',
        'backend\themes\insignia\assets\plugins\inputmask\Inputmask',
    ];
}