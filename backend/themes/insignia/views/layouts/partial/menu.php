<?php

use backend\themes\insignia\assets\Insignia;
use backend\components\widgets\Menu;

$asset = Insignia::register($this);
?>
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <?= Menu::widget([
            'activateItems' => true,
            'activateParents' => true,
            'options' => [
                'id' => 'side-menu',
                'class' => 'nav metismenu'
            ],
            "submenuTemplate" => "\n<ul class='nav nav-second-level collapse'>\n{items}\n</ul>\n",
            'items' => [
                [
                    'label' => 'Публикации',
                    'url' => ['/item/item/index'],
                    'icon' => 'address-card',
                    'inner' => true,
                    'items' => [
                        [
                            'label' => 'Все публикации',
                            'url' => ['/item/item/index']
                        ],
                        [
                            'label' => 'Добавить публикацию',
                            'url' => ['/item/item/create']
                        ],
                    ],
                ],
                [
                    'label' => 'Типы',
                    'url' => ['/item/type/index'],
                    'icon' => 'bookmark',
                    'inner' => true,
                    'items' => [
                        [
                            'label' => 'Все типы',
                            'url' => ['/item/type/index'],
                        ],
                        [
                            'label' => 'Добавить тип',
                            'url' => ['/item/type/create']
                        ],
                    ],
                ],
                [
                    'label' => 'Источники',
                    'url' => ['/item/source/index'],
                    'icon' => 'folder',
                    'inner' => true,
                    'items' => [
                        [
                            'label' => 'Все источники',
                            'url' => ['/item/source/index'],
                        ],
                        [
                            'label' => 'Добавить источник',
                            'url' => ['/item/source/create']
                        ],
                    ],
                ],
                [
                    'label' => 'Авторы',
                    'url' => ['/item/author/index'],
                    'icon' => 'user',
                    'inner' => true,
                    'items' => [
                        [
                            'label' => 'Все авторы',
                            'url' => ['/item/author/index'],
                        ],
                        [
                            'label' => 'Добавить автора',
                            'url' => ['/item/author/create']
                        ],
                    ],
                ],
                [
                    'label' => 'Профиль',
                    'url' => ['/user/item/profile'],
                    'icon' => 'id-badge',
                ],
                [
                    'label' => 'Пользователи',
                    'url' => ['/user/item/index'],
                    'icon' => 'users',
                    'inner' => true,
                    'visible' => IDENTITY()->type == USER_TYPE_ADMIN,
                    'items' => [
                        [
                            'label' => 'Все пользователи',
                            'url' => ['/user/item/index'],
                        ],
                        [
                            'label' => 'Добавить пользователя',
                            'url' => ['/user/item/create']
                        ],
                    ],
                ],
                [
                    'label' => 'Настройки',
                    'url' => ['/settings/index'],
                    'icon' => 'gear',
                    'inner' => false,
                    'visible' => IDENTITY()->type == USER_TYPE_ADMIN,
                ],
            ],
        ]); ?>
    </div>
</nav>