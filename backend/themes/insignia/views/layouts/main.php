<?php

use yii\helpers\Html;
use backend\themes\insignia\assets\Insignia;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$asset = Insignia::register($this);

$this->beginPage();
?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="<?= Yii::$app->charset; ?>">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?= Html::csrfMetaTags(); ?>
        <title><?= $this->title; ?></title>
        <?php
        $this->head();
        ?>
    </head>
    <body class="md-skin fixed-sidebar fixed-nav">
    <?php $this->beginBody(); ?>
    <div id="wrapper">
        <?= $this->render("./partial/menu"); ?>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-fixed-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                            <i class="fa fa-bars"></i> </a>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a class="username" href="<?= Url::to(['/user/item/profile']); ?>">
                                <?= Yii::t('app', 'Добро пожаловать, ') . Yii::$app->user->identity->baseUserData->name ?>
                            </a>
                        </li>
                        <li class="delimiter">
                            <a href="">|</a>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/user/auth/logout']); ?>">
                                <i class="fa fa-sign-out" aria-hidden="true"></i>
                                <?= Yii::t("app", "Выход"); ?>
                            </a>
                        </li>
                    </ul>

                </nav>
            </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-12">
                    <h2><?= $this->title; ?></h2>
                    <?php
                    $breadcrumbs = !empty($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [];
                    if (!empty($breadcrumbs)) {
                        if (is_string($breadcrumbs[count($breadcrumbs) - 1]))
                            $breadcrumbs[count($breadcrumbs) - 1] = [
                                "label" => $breadcrumbs[count($breadcrumbs) - 1]
                            ];
                    }
                    echo Breadcrumbs::widget([
                        'itemTemplate' => '<li>{link}</li>',
                        'options' => [
                            'class' => 'breadcrumb',
                            'tag' => 'ol'
                        ],
                        'homeLink' => false,
                        'links' => $breadcrumbs
                    ]);
                    ?>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="wrapper wrapper-content animated fadeInRight">
                        <?= $content; ?>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div>
                    ©Нефтекамский филиал БашГУ 2000-2018.
                    Все права защищены.
                </div>
            </div>
        </div>
    </div>
    <div class="modal inmodal modal-scroll fade" id="ajax-view" role="basic" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeInDown">
                <span> &nbsp;<?= Yii::t("app", "Загрузка данных") ?></span>
            </div>
        </div>
    </div>
    <?php $this->endBody(); ?>
    </body>
    </html>
<?php
$this->endPage();