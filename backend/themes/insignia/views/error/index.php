<?php

use yii\helpers\Html;
use backend\themes\insignia\assets\Insignia;

$asset = Insignia::register($this);

$this->beginPage();
?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="<?= Yii::$app->charset; ?>">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?= Html::csrfMetaTags(); ?>
        <title><?= $statusCode ?></title>
        <?php
        $this->head();
        ?>
    </head>
    <body class="gray-bg">
    <?php $this->beginBody(); ?>
    <div class="middle-box text-center animated fadeInDown">
        <h1><?= $statusCode; ?></h1>
        <h3 class="font-bold"><?= $name; ?></h3>

        <div class="error-desc">
            <?= $message; ?>
            <br/>
            <a href="javascript:history.go(-1);" class="btn btn-primary m-t"><?= Yii::t("app", "Назад"); ?></a>
        </div>
    </div>

    <?php $this->endBody(); ?>
    </body>
    </html>
<?php
$this->endPage();