<?php

/* @var $this \yii\web\View */
/* @var $model \common\models\db\Settings */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Settings;


$this->title = Yii::t("app", "Настройки");
$this->params["breadcrumbs"][] = [
    "label" => $this->title
];
?>

<div class="row">
    <div class="col-sm-12">
        <div class="ibox">
            <div class="ibox-content">
                <div class="item-form">

                    <?php $form = ActiveForm::begin(); ?>
                    <?php
                    if ($model->hasErrors()) {
                        echo $form->errorSummary($model, ['class' => 'alert alert-danger']);
                    }
                    if (($success = Yii::$app->session->getFlash('saveSettings', false, true)) == true) {
                        ?>
                        <div class="alert alert-success">
                            <?= Yii::t('app', 'Изменения успешно сохранены') ?>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'autofocus' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'server')->textInput(['maxlength' => true, 'autofocus' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'port')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'encrypt')->dropDownList([
                                EMAIL_ENCRYPT_NULL => Settings::encryptName(EMAIL_ENCRYPT_NULL),
                                EMAIL_ENCRYPT_SSL => Settings::encryptName(EMAIL_ENCRYPT_SSL),
                                EMAIL_ENCRYPT_TLS => Settings::encryptName(EMAIL_ENCRYPT_TLS)
                            ])
                            ?>
                        </div>
                    </div>

                    <div class="form-group text-right">
                        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>

