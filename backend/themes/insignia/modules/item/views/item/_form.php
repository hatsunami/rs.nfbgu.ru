<?php

use common\models\db\Item;
use common\models\db\Source;
use common\models\db\Type;
use common\models\db\Author;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \backend\modules\item\forms\Item */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
    <div class="col-sm-12">
        <div class="ibox">
            <div class="ibox-content">
                <div class="item-form">

                    <?php $form = ActiveForm::begin([
                            "options" => [
                                    "class" => "publish-form"
                            ]
                    ]); ?>

                    <?php
                    if ($model->hasErrors()) {
                        echo $form->errorSummary($model, ['class' => 'alert alert-danger']);
                    }
                    ?>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'title')->textInput(['maxlength' => true, 'autofocus' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form
                                ->field($model, 'type', [
                                    'template' => '
                                        {label}
                                        <div class="input-group">
                                            {input}
                                            <span class="input-group-btn">
                                                ' . Html::a(
                                                    "+",
                                                    ["/item/type/create", "target" => "type-list"],
                                                    [
                                                        "class" => "btn btn-primary",
                                                        "data" => [
                                                            "toggle" => "modal",
                                                            "target" => "#ajax-view",
                                                        ]
                                                    ]
                                                ) . '
                                            </span>
                                        </div>
                                        {hint}{error}
                                    '
                                ])
                                ->dropDownList(
                                    ArrayHelper::map(
                                        Type::find()
                                            ->where(['is_deleted' => IS_NOT_DELETED])
                                            ->orderBy(['name' => SORT_DESC])
                                            ->all(),
                                        'id', 'name'
                                    ), [
                                        'data' => [
                                            'target' => 'type-list',
                                        ]
                                    ]
                                );
                            ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <?= $form
                                ->field($model, 'source_id', [
                                    'template' => '
                                        {label}
                                        <div class="input-group">
                                            {input}
                                            <span class="input-group-btn">
                                                ' . Html::a(
                                            "+",
                                            ["/item/source/create", "target" => "source-list"],
                                            [
                                                "class" => "btn btn-primary",
                                                "data" => [
                                                    "toggle" => "modal",
                                                    "target" => "#ajax-view",
                                                ]
                                            ]
                                        ) . '
                                            </span>
                                        </div>
                                        {hint}{error}
                                    '
                                ])
                                ->dropDownList(
                                    ArrayHelper::map(
                                        Source::find()
                                            ->where(['is_deleted' => IS_NOT_DELETED])
                                            ->orderBy(['name' => SORT_DESC])
                                            ->all(),
                                        'id', 'name'
                                    ), [
                                    'data' => [
                                        'target' => 'source-list',
                                    ]
                                ]);
                            ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'year')->dropDownList(
                                array_combine(range(date('Y'), 1972, -1), range(date('Y'), 1972, -1))
                            );
                            ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'half_year')->dropDownList([
                                HALF_YEAR_FIRST => Item::halfYearName(HALF_YEAR_FIRST),
                                HALF_YEAR_SECOND => Item::halfYearName(HALF_YEAR_SECOND),
                            ])
                            ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'first_page')->textInput([
                                'data' => [
                                    'action' => 'calculate-volume',
                                ]
                            ]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'last_page')->textInput([
                                'data' => [
                                    'action' => 'calculate-volume',
                                ]
                            ]) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'volume')->textInput([
                                'data' => [
                                    'target' => 'result-volume',
                                ]
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form
                                ->field($model, 'authors', [
                                    'template' => '
                                        {label}
                                        <div class="input-group">
                                            {input}
                                            <span class="input-group-btn">
                                                ' . Html::a(
                                                    "+",
                                                    ["/item/author/create", "target" => "author-list"],
                                                    [
                                                        "class" => "btn btn-primary",
                                                        "data" => [
                                                            "toggle" => "modal",
                                                            "target" => "#ajax-view",
                                                        ]
                                                    ]
                                                ) . '
                                            </span>
                                        </div>
                                        {hint}{error}
                                    '
                                ])
                                ->dropDownList(
                                    ArrayHelper::map(
                                        Author::find()
                                            ->where(['is_deleted' => IS_NOT_DELETED])
                                            ->orderBy(['name' => SORT_DESC])
                                            ->all(),
                                        'id', 'name'
                                    ), [
                                    'multiple' => true,
                                    'data' => [
                                        'target' => 'author-list',
                                    ]
                                ]);
                            ?>
                        </div>
                    </div>

                    <div class="form-group text-right">
                        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
