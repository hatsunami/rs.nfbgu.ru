<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\db\Item */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Публикации'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-sm-12">
        <div class="ibox <?= Yii::$app->request->isAjax ? 'ajax' : null; ?>">
            <div class="ibox-content">

                <div class="item-view">

                    <h1><?= Html::encode($this->title) ?></h1>

                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'title',
                            [
                                'attribute' => 'type',
                                'value' => $model->baseType->name,
                            ],
                            [
                                'attribute' => 'source_id',
                                'value' => $model->baseSource->name ?? null,
                                'label' => Yii::t('app', 'Ресурс'),
                            ],
                            'first_page',
                            'last_page',
                            'volume',
                            'year',
                            [
                                'attribute' => 'half_year',
                                'value' => $model->halfYearName,
                            ],
                            [
                                'label' => Yii::t("app", "Авторы"),
                                'visible' => count($model->baseAuthors),
                                'value' => function($model){
                                    $authors = $model->baseAuthors;
                                    if (count($authors)){
                                        return implode(", ", array_values(
                                            ArrayHelper::map(
                                                $authors, "id", function($in){
                                                    return $in->baseAuthor ? $in->baseAuthor->name : null;
                                                }
                                            )
                                        ));
                                    }
                                }
                            ],
                            [
                                'attribute' => 'created_at',
                                'value' => date("d.m.Y H:i", $model->created_at)
                            ],
                            [
                                'attribute' => 'created_at',
                                'value' => date("d.m.Y H:i", $model->updated_at)
                            ],
                        ],
                    ]) ?>
                    <?php
                    if ($model->user == Yii::$app->user->identity->id){
                        ?>
                        <p>
                            <?= Html::a(Yii::t('app', 'Изменить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

                            <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Вы действительно хотите удалить данную публикацию?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </p>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
