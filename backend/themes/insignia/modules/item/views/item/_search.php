<?php

use backend\components\widgets\yii2export\ExportFile;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>
    <div class="row">
        <div class="col-lg-9 col-md-8 col-sm-6 col-xs-12">
            <?php
            $form = ActiveForm::begin([
                'method' => 'get',
                'action' => Url::to([
                    'index',
                ])
            ]);
            ?>

            <?= $form
                ->field($model, 'query', [
                    'template' => '
                        <div class="input-group">
                               {input}
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-search"></i> ' . Yii::t('app', 'Поиск') . '
                                </button>
                            </span>
                        </div>
                        {hint}{error}
                        '
                ])
                ->input('search', [
                    'placeholder' => Yii::t('app', 'Введите текст для поиска')
                ]); ?>

            <?php
            echo $this->render('_filter', [
                    'model' => $model,
                'form' => $form
            ]);
            ActiveForm::end();

            ?>
        </div>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 text-right">
            <?php
            echo ExportFile::widget([
                'model' => 'common\models\data\item\searches\ItemSearch',   // путь к модели
                'title'             => 'Заголовок документа',
                'queryParams'       => Yii::$app->request->queryParams,

                'getAll'            => true,                               // все записи - true, учитывать пагинацию - false
                'csvCharset'        => 'Windows-1251',                      // кодировка csv файла: 'UTF-8' (по умолчанию) или 'Windows-1251'

                'buttonClass'       => 'btn btn-primary',                   // класс кнопки
                'blockClass'        => 'pull-left',                         // класс блока в котором кнопка
                'blockStyle'        => 'padding: 5px;',                     // стиль блока в котором кнопка

                // экспорт в следующие файлы (true - разрешить, false - запретить)
                'xls'               => true,
                'csv'               => false,
                'word'              => true,
                'html'              => true,
                'pdf'               => false,

                // шаблоны кнопок
                'xlsButtonName'     => Yii::t('app', 'MS Excel'),
//                'csvButtonName'     => Yii::t('app', 'CSV'),
                'wordButtonName'    => Yii::t('app', 'MS Word'),
                'htmlButtonName'    => Yii::t('app', 'HTML'),
//                'pdfButtonName'     => Yii::t('app', 'PDF'),
            ])
            ?>
        </div>
    </div>
