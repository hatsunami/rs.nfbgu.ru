<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Публикации');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-sm-12">
        <div class="ibox">
            <div class="ibox-content">
                <div class="item-index">

                    <?php Pjax::begin([
                        "id" => "pjax-data"
                    ]); ?>
                    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    <?= Html::beginForm(['delete'], 'post', [
                        'data' => [
                            'pjax' => false,
                        ]
                    ]) ?>
                    <div class="row">
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <?= Html::a(Yii::t('app', 'Добавить публикацию'), ['create'], ['class' => 'btn btn-success']) ?>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12 text-right">
                            <?php
                                $delButton = Html::submitButton(Yii::t('app', 'Удалить'), [
                                    'class' => 'btn btn-danger hidden',
                                    'data' => [
                                        'pjax' => false,
                                        'confirm' => 'Вы действительно хотите удалить эту(-и)
                                         публикацию(-ии)?',
                                        'method' => 'post',
                                        'action' => 'remove-button'
                                    ]
                                ]);
                            ?>
                            <?= $delButton ?>
                        </div>
                    </div>
                    <br>
                    <?php
                        $template = '{items}{summary}<div class="row"><div class="col-md-8 col-sm-6 col-xs-12">{pager}</div><div class="col-md-4 col-sm-6 col-xs-12 text-right">' .$delButton. '</div></div>'
                    ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            [
                                'name' => 'id',
                                'class' => 'backend\components\grid\CheckBoxColumn',
                                'checkboxOptions' => function ($model) {
                                    return [
                                        'name' => 'id[]',
                                        'value' => $model->id,
                                        'hidden' => $model->user != Yii::$app->user->identity->id
                                    ];
                                },
                                'contentOptions' => [
                                    'style' => [
                                        'width' => '60px',
                                    ]
                                ],
                                'headerOptions' => [
                                    'style' => [
                                        'width' => '60px',
                                    ]
                                ],

                            ],
                            [
                                'attribute' => 'title',
                                'value' => function ($model) {
                                    return Html::a($model->title, [
                                        'view', 'id' => $model->id
                                    ], [
                                        'data' => [
                                            'toggle' => 'modal',
                                            'target' => '#ajax-view',
                                        ],

                                    ]);
                                },
                                'format' => 'raw',
                            ],
                            [
                                'attribute' => 'type',
                                'value' => function ($model) {
                                    return $model->baseType->name ?? null;
                                }
                            ],
                            [
                                'attribute' => 'source',
                                'value' => function ($model) {
                                    return $model->baseSource->name ?? null;
                                },
                                'label' => Yii::t('app', 'Ресурс')
                            ],
                            [
                                'attribute' => 'pages',
                                'label' => Yii::t('app', 'Страницы'),
                                'value' => function ($model) {
                                    return implode("-", [
                                        $model->first_page,
                                        $model->last_page
                                    ]);
                                }
                            ],
                            'volume',
                            'year',
                            [
                                'attribute' => 'half_year',
                                'value' => function ($model) {
                                    return $model->halfYearName;
                                },
                            ],
                        ],
                        'layout' => $template,
                        'tableOptions' => [
                            'class' => 'table table-striped table-hover'
                        ],
                        'formatter' => [
                            'class' => 'yii\i18n\Formatter',
                            'nullDisplay' => '',
                        ]
                    ]); ?>
                    <?php Html::endForm(); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
