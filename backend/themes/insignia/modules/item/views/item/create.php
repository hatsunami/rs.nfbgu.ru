<?php

/* @var $this yii\web\View */
/* @var $model common\models\db\Item */

$this->title = Yii::t('app', 'Добавить публикацию');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Публикации'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
