<?php

/* @var $this yii\web\View */
/* @var $model common\models\db\Item */

$this->title = Yii::t('app', 'Обновление данных');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Публикации'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
