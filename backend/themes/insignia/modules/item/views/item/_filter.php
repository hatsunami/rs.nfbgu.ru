<?php

use common\models\db\Item;
use common\models\db\Source;
use common\models\db\Type;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>
<div class="modal inmodal modal-scroll fade" id="filter-view" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeInDown">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                            class="sr-only"><= Yii::t("app", "Закрыть"); ?></span></button>
                <h4 class="modal-title">Фильтрация данных</h4>
                <small class="font-bold">Здесь вы можете отфилтровать публикации по определнным параметрам.</small>
                <br>
                <small class="font-bold text-danger">При экспорте страницы буду проэкспортированы отфильтрованные
                    данные!
                </small>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <?= $form
                            ->field($model, 'type')
                            ->dropDownList(
                                ArrayHelper::map(
                                    Type::find()
                                        ->where(['is_deleted' => IS_NOT_DELETED])
                                        ->orderBy(['name' => SORT_DESC])
                                        ->all(),
                                    'id', 'name'
                                ), [
                                    'data' => [
                                        'target' => 'type-list',
                                    ],
                                    'prompt' => Yii::t("app", 'Не важно')
                                ]
                            );
                        ?>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <?= $form
                            ->field($model, 'source_id')
                            ->dropDownList(
                                ArrayHelper::map(
                                    Source::find()
                                        ->where(['is_deleted' => IS_NOT_DELETED])
                                        ->orderBy(['name' => SORT_DESC])
                                        ->all(),
                                    'id', 'name'
                                ), [
                                'data' => [
                                    'target' => 'source-list',
                                ],
                                'prompt' => Yii::t("app", 'Не важно')

                            ]);
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <?= $form->field($model, 'first_page')->textInput([
                            "data" => [
                                "type" => "number"
                            ],
                            "placeholder" => 0
                        ]) ?>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <?= $form->field($model, 'last_page')->textInput([
                            "data" => [
                                "type" => "number"
                            ],
                            "placeholder" => 0
                        ]) ?>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <?= $form->field($model, 'startVolume')->textInput([
                            "data" => [
                                "type" => "decimal"
                            ],
                            "placeholder" => 0
                        ]) ?>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <?= $form->field($model, 'endVolume')->textInput([
                            "data" => [
                                "type" => "decimal"
                            ],
                            "placeholder" => 0
                        ]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <?= $form->field($model, 'startYear')->dropDownList(
                            array_combine(range(date('Y'), 1972, -1), range(date('Y'), 1972, -1)), [
                                'prompt' => Yii::t("app", 'Не важно')
                            ]
                        );
                        ?>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <?= $form->field($model, 'endYear')->dropDownList([
                            array_combine(range(date('Y'), 1972, -1), range(date('Y'), 1972, -1))], [
                                'prompt' => Yii::t("app", 'Не важно')
                            ]
                        );
                        ?>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <?= $form->field($model, 'half_year')->dropDownList([
                            HALF_YEAR_FIRST => Item::halfYearName(HALF_YEAR_FIRST),
                            HALF_YEAR_SECOND => Item::halfYearName(HALF_YEAR_SECOND),
                        ], [
                            'prompt' => Yii::t("app", 'Не важно')
                        ])
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <?= $form->field($model, 'startDate')->textInput([
                                "data" => [
                                    "type" => "date"
                                ]
                        ])?>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <?= $form->field($model, 'endDate')->textInput([
                            "data" => [
                                "type" => "date"
                            ]
                        ]) ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary"><?= Yii::t('app', 'Отфильтровать'); ?></button>
                <?= Html::a('Сброс', ['/item/item'], [
                    'class' => 'btn btn-danger',
                    'data-pjax' => 0
                ]); ?>

            </div>
        </div>
    </div>
</div>