<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\db\Type */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="source-form">
    <?php
    if (REQUEST()->isAjax){
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">&times;</span>
        </button>
        <h4 class="modal-title"><?= $this->title ?></h4>
    </div>
    <div class="modal-body">
        <?php
        }

        $form = ActiveForm::begin([
            "action" => Url::to(["/item/type/create"]),
            "method" => "post",
            "enableAjaxValidation" => false,
            "id" => REQUEST()->isAjax ? "ajax-validate-form" : null,
            "options" => [
                "data" => [
                    "target" => REQUEST()->getQueryParam("target")
                ]
            ]
        ]); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'autofocus' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
        </div>
        <?php
        ActiveForm::end();
        if (REQUEST()->isAjax){
        ?>
    </div>
<?php
}
?>

</div>