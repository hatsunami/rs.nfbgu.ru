<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\item\searches\TypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Типы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-sm-12">
        <div class="ibox">
            <div class="ibox-content">
                <div class="type-index">

                    <?php Pjax::begin([
                        "id" => "pjax-data",
                    ]); ?>
                    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                    <?= Html::beginForm(['delete'], 'post', [
                        'data' => [
                            'pjax' => 0,

                        ]
                    ]) ?>
                    <div class="row">
                        <div class="col-md-6">
                            <?= Html::a(Yii::t('app', 'Добавить тип'), ['create'], ['class' => 'btn btn-success']) ?>
                        </div>
                        <div class="col-md-6 text-right">
                            <?= Html::submitButton(Yii::t('app', 'Удалить'), [
                                'class' => 'btn btn-danger hidden',
                                'data' => [
                                    'pjax' => false,
                                    'confirm' => 'Вы действительно хотите удалить этот(-и) тип(-ы)?',
                                    'method' => 'post',
                                    'action' => 'remove-button'
                                ]
                            ]) ?>
                        </div>
                    </div>
                    <br>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            [
                                'name' => 'id',
                                'class' => 'backend\components\grid\CheckBoxColumn',
                                'checkboxOptions' => function ($model) {
                                    return [
                                        'name' => 'id[]',
                                        'value' => $model->id,
                                        'hidden' => $model->user != Yii::$app->user->identity->id
                                    ];
                                },
                                'contentOptions' => [
                                    'style' => [
                                        'width' => '60px',
                                    ]
                                ],
                                'headerOptions' => [
                                    'style' => [
                                        'width' => '60px',
                                    ]
                                ],
                            ],
                            [
                                'attribute' => 'name',
                                'value' => function ($model) {
                                    return Html::a($model->name, [
                                        'view', 'id' => $model->id
                                    ], [
                                        'data' => [
                                            'toggle' => 'modal',
                                            'target' => '#ajax-view',
                                        ],

                                    ]);
                                },
                                'format' => 'raw',
                            ]
                        ],
                        'layout' => '{items}{summary}{pager}',
                        'tableOptions' => [
                            'class' => 'table table-striped table-hover'
                        ],
                        'formatter' => [
                            'class' => 'yii\i18n\Formatter',
                            'nullDisplay' => '',
                        ]
                    ]); ?>
                    <?= Html::endForm() ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

