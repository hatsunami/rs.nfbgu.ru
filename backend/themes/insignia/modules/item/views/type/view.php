<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\db\Type */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Типы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-sm-12">
        <div class="ibox <?= Yii::$app->request->isAjax ? 'ajax' : null; ?>">
            <div class="ibox-content">
                <div class="type-view">
                    <h2 class="modal-title"><?= Yii::$app->request->isAjax ? $this->title : null; ?></h2>
                    <br>
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'name',
                            'created_at:datetime',
                            'updated_at:datetime',
                        ],
                    ]) ?>

                    <?php
                    if ($model->user == Yii::$app->user->identity->id) {
                        ?>
                        <p>
                            <?= Html::a(Yii::t('app', 'Изменить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Вы действительно хотите удалить этот тип?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </p>
                        <?php
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>
