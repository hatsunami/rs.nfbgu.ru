<?php

/* @var $this yii\web\View */
/* @var $model common\models\db\Type */

$this->title = Yii::t('app', 'Добавление типа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Типы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
if (!REQUEST()->isAjax) {
    ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="type-create">
        <?php
        }
            echo $this->render('_form', [
                'model' => $model,
            ]);
        if (!REQUEST()->isAjax) {
        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>