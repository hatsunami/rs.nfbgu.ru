<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\db\Author;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\item\searches\AuthorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Авторы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-sm-12">
        <div class="ibox">
            <div class="ibox-content">
                <div class="author-index">
                    <?php Pjax::begin([
                        "id" => "pjax-data",
                    ]); ?>
                    <?= $this->render('_search', ['model' => $searchModel]); ?>
                    <?= Html::beginForm(['delete'], 'post', [
                        'id' => 'author-form'
                    ]) ?>

                    <div class="row">
                        <div class="col-md-6">
                            <?= Html::a(Yii::t('app', 'Добавить автора'), ['create'], ['class' => 'btn btn-success']) ?>
                        </div>
                        <div class="col-md-6 text-right">
                            <?= Html::submitButton(Yii::t('app', 'Удалить'), [
                                'class' => 'btn btn-danger hidden',
                                'data' => [
                                    'pjax' => false,
                                    'confirm' => Yii::t("app", 'Вы действительно хотите удалить этого(-их) автора(-ов)?'),
                                    'method' => 'post',
                                    'action' => 'remove-button'
                                ]
                            ]) ?>
                        </div>
                    </div>
                    <br>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'name' => 'id',
                                'class' => 'backend\components\grid\CheckBoxColumn',
                                'checkboxOptions' => function ($model) {
                                    return [
                                        'name' => 'id[]',
                                        'value' => $model->id,
                                        'hidden' => $model->user != Yii::$app->user->identity->id
                                    ];
                                },
                                'contentOptions' => [
                                    'style' => [
                                        'width' => '60px',
                                    ]
                                ],
                                'headerOptions' => [
                                    'style' => [
                                        'width' => '60px',
                                    ]
                                ],
                            ],
                            [
                                'attribute' => 'name',
                                'value' => function ($model) {
                                    return Html::a($model->name, [
                                        'view', 'id' => $model->id
                                    ], [
                                        'data' => [
                                            'toggle' => 'modal',
                                            'target' => '#ajax-view',
                                        ],

                                    ]);
                                },
                                'format' => 'raw',
                                'filter' => false,
                            ],
                            [
                                'attribute' => 'type',
                                'value' => function($model){
                                    return $model->typeName;
                                },
                                'filter' => Html::activeDropDownList($searchModel, 'type', [
                                    AUTHOR_TYPE_TEACHER => Author::typeName(AUTHOR_TYPE_TEACHER),
                                    AUTHOR_TYPE_STUDENT => Author::typeName(AUTHOR_TYPE_STUDENT),
                                ], [
                                    'prompt' => Yii::t("app", "Не важно"),
                                    'class' => 'form-control'
                                ]),
                                'contentOptions' => [
                                    'style' => [
                                        'width' => '250px',
                                    ]
                                ],
                                'headerOptions' => [
                                    'style' => [
                                        'width' => '250px',
                                    ]
                                ],
                            ],
                        ],
                        'layout' => '{items}{summary}{pager}',
                        'tableOptions' => [
                            'class' => 'table table-striped table-hover'
                        ],
                        'formatter' => [
                            'class' => 'yii\i18n\Formatter',
                            'nullDisplay' => '',
                        ]
                    ]); ?>

                    <?php Html::endForm(); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
