<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\db\Author;

/* @var $this yii\web\View */
/* @var $model common\models\db\Author */
/* @var $form yii\widgets\ActiveForm */

if (REQUEST()->isAjax){
?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">&times;</span>
        </button>
        <h4 class="modal-title"><?= $this->title ?></h4>
    </div>
    <div class="modal-body">
    <?php
    }
        $form = ActiveForm::begin([
            "action" => Url::to(["/item/author/create"]),
            "method" => "post",
            "enableAjaxValidation" => false,
            "id" => REQUEST()->isAjax ? "ajax-validate-form" : null,
            "options" => [
                "data" => [
                    "target" => REQUEST()->getQueryParam("target")
                ]
            ]
        ]);
        if ($model->hasErrors()) {
            echo $form->errorSummary($model, [
                'class' => 'alert alert-danger',
            ]);
        }
        ?>
        <div class="row">
            <div class="col-md-8">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'autofocus' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'type')->dropDownList([
                    AUTHOR_TYPE_TEACHER => Author::typeName(AUTHOR_TYPE_TEACHER),
                    AUTHOR_TYPE_STUDENT => Author::typeName(AUTHOR_TYPE_STUDENT),
                ])->label(Yii::t('app', 'Тип автора'))
                ?>
            </div>
        </div>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php
        ActiveForm::end();
        if (REQUEST()->isAjax){
        ?>
    </div>
<?php
}
?>
</div>
