<?php

/* @var $this yii\web\View */
/* @var $model common\models\db\Source */

$this->title = Yii::t('app', 'Добавление источника');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Источники'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
if (!REQUEST()->isAjax){
?>
<div class="row">
    <div class="col-sm-12">
        <div class="ibox">
            <div class="ibox-content">
                <div class="source-create">
<?php
}
                    echo $this->render('_form', [
                        'model' => $model,
                    ]);
if (!REQUEST()->isAjax) {
    ?>
                </div>
            </div>
        </div>
    </div>
</div>
    <?php
}
?>