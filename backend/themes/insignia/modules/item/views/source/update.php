<?php

/* @var $this yii\web\View */
/* @var $model common\models\db\Source */

$this->title =Yii::t('app', 'Обновление');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Источники'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="source-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
