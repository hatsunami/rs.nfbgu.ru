<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$form = ActiveForm::begin([
    'method' => 'get',
    'action' => Url::to([
        'index',
    ])
]);
echo $form
    ->field($model, 'name', [
        'template' => '
            <div class="input-group">
                   {input}
                <span class="input-group-btn">
                    <button type="submit" class="btn btn btn-primary">
                        <i class="fa fa-search"></i> ' . Yii::t('app', 'Поиск') . '
                    </button>
                </span>
            </div>
            {hint}{error}
            '
    ])
    ->input('search', [
        'placeholder' => Yii::t('app', 'Введите текст для поиска')
    ]);
ActiveForm::end();

?>