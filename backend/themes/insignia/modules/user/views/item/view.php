<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\db\User */

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Пользователи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-sm-12">
        <div class="ibox <?= Yii::$app->request->isAjax ? 'ajax' : null; ?>">
            <div class="ibox-content">
                <div class="user-view">

                    <h2 class="modal-title"><?= Yii::$app->request->isAjax ? $this->title : null; ?></h2>
                    <br>

                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'baseUserData.name',
                            'email:email',
                            'password',
                            'place',
                            [
                                'attribute' => 'type',
                                'value' => function ($model) {
                                    if ($model->type == USER_TYPE_ADMIN) {
                                        return Html::label($model->typeName, null, [
                                            'class' => 'text-danger',
                                        ]);
                                    } else if ($model->type == USER_TYPE_MANAGER){
                                        return Html::label($model->typeName, null, [
                                            'class' => 'text-warning',
                                        ]);
                                    } else {
                                        return Html::label($model->typeName, null, [
                                            'class' => 'text-info',
                                        ]);
                                    }
                                },
                                'format' => 'raw'
                            ],
                            [
                                'attribute' => 'state',
                                'contentOptions' => [
                                    'class' => 'client-status',
                                ],
                                'value' => function ($model) {
                                    if ($model->state == STATE_INACTIVE) {
                                        return Html::label(Yii::t('app', 'В ожидании'), null, [
                                            'class' => 'label label-warning',
                                        ]);
                                    } else {
                                        return Html::label(Yii::t('app', 'Активен'), null, [
                                            'class' => 'label label-primary',
                                        ]);
                                    }
                                },
                                'format' => 'raw'
                            ],
                            'created_at:datetime',
                            'updated_at:datetime',
                        ],
                    ]) ?>
                    <p>

                        <?= Html::a(Yii::t('app', 'Изменить'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?php
                        if ($model->id != Yii::$app->user->identity->id) {
                            ?>
                            <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Вы действительно хотите удалить этого пользователя?'),
                                    'method' => 'post',
                                ],
                            ]) ?>
                            <?php
                        }
                        ?>
                    </p>

                </div>
            </div>
        </div>
    </div>
</div>
