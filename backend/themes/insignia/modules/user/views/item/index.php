<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Пользователи');
$this->params["breadcrumbs"][] = $this->title

?>
<div class="row">
    <div class="col-sm-12">
        <div class="ibox">
            <div class="ibox-content">
                <?php Pjax::begin([
                    "id" => "pjax-data"
                ]); ?>
                <p>
                    Все пользователи должны быть авторизованы администратором.
                </p>
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                <?= Html::beginForm(['delete'], 'post', [
                    'data' => [
                        'pjax' => false,
                    ]
                ]) ?>
                <div class="row">
                    <div class="col-md-8 col-sm-6 col-xs-12">
                        <?= Html::a(Yii::t('app', 'Добавить пользователя'), ['create'], ['class' => 'btn btn-success']) ?>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 text-right">
                        <?php
                        $delButton = Html::submitButton(Yii::t('app', 'Удалить'), [
                            'class' => 'btn btn-danger hidden',
                            'data' => [
                                'pjax' => false,
                                'confirm' => 'Вы действительно хотите удалить эту(-и)
                                         публикацию(-ии)?',
                                'method' => 'post',
                                'action' => 'remove-button'
                            ]
                        ]);
                        ?>
                        <?= $delButton ?>
                    </div>
                </div>
                <div class="clients-list">
                    <div class="full-height-scroll">
                        <div class="table-responsive">
                            <?php
                            echo
                            GridView::widget([
                                'dataProvider' => $dataProvider,
                                'showHeader' => false,
                                'columns' => [
                                    [
                                        'name' => 'id',
                                        'class' => 'backend\components\grid\CheckBoxColumn',
                                        'checkboxOptions' => function ($model) {
                                            return [
                                                'name' => 'id[]',
                                                'value' => $model->id,
                                                'hidden' => $model->id == Yii::$app->user->identity->id
                                            ];
                                        },
                                        'contentOptions' => [
                                            'style' => [
                                                'width' => '60px',
                                            ]
                                        ],
                                        'headerOptions' => [
                                            'style' => [
                                                'width' => '60px',
                                            ]
                                        ],
                                    ],
                                    [
                                        'attribute' => 'email',
                                        'value' => function ($model) {
                                            return Html::a('<i class="fa fa-envelope"></i> ' . $model->email, [
                                                'view', 'id' => $model->id
                                            ], [
                                                'data' => [
                                                    'toggle' => 'modal',
                                                    'target' => '#ajax-view',
                                                ],

                                            ]);
                                        },
                                        'format' => 'raw',
                                    ],
                                    'baseUserData.name',
                                    [
                                        'contentOptions' => [
                                            'class' => 'client-status',
                                        ],
                                        'value' => function ($model) {
                                            if ($model->state == STATE_INACTIVE) {
                                                return Html::label(Yii::t('app', 'В ожидании'), null, [
                                                    'class' => 'label label-warning',
                                                ]);
                                            } else {
                                                return Html::label(Yii::t('app', 'Активен'), null, [
                                                    'class' => 'label label-primary',
                                                ]);
                                            }
                                        },
                                        'format' => 'raw'
                                    ]
                                ],
                                'layout' => '{items}{summary}{pager}',
                                'tableOptions' => [
                                    'class' => 'table table-striped table-hover'
                                ],
                                'formatter' => [
                                    'class' => 'yii\i18n\Formatter',
                                    'nullDisplay' => '',
                                ]
                            ]) ?>
                            <?php Html::endForm(); ?>
                            <?php Pjax::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>