<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\db\User;

/* @var $this yii\web\View */
/* @var $model common\models\db\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
    <div class="col-sm-12">
        <div class="ibox">
            <div class="ibox-content">
                <div class="user-form">

                    <?php $form = ActiveForm::begin(); ?>
                    <?php
                    if ($model->hasErrors()) {
                        echo $form->errorSummary($model, ['class' => 'alert alert-danger']);
                    }
                    if (($success = Yii::$app->session->getFlash('successChange', false, true)) == true) {
                        ?>
                        <div class="alert alert-success">
                            <?= Yii::t('app', 'Изменения успешно сохранены') ?>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'name')->textInput([
                                'maxlength' => true,
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'email')->input('email', [
                                'maxlength' => true,
                                'readonly' => !$model->isNewRecord
                            ]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                        if (IDENTITY()->type == USER_TYPE_ADMIN) {
                            ?>
                            <div class="col-md-6">
                                <?= $form->field($model, 'type')->dropDownList([
                                    USER_TYPE_CLIENT => User::typeName(USER_TYPE_CLIENT),
                                    USER_TYPE_MANAGER => User::typeName(USER_TYPE_MANAGER),
                                    USER_TYPE_ADMIN => User::typeName(USER_TYPE_ADMIN),
                                ])
                                ?>
                            </div>
                            <?php
                        }
                        ?>
                        <?php
                        if (!$model->isNewRecord) {
                            ?>
                            <div class="col-md-6">
                                <?= $form->field($model, 'state')->dropDownList([
                                    STATE_ACTIVE => User::stateName(STATE_ACTIVE),
                                    STATE_INACTIVE => User::stateName(STATE_INACTIVE),
                                ])
                                ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-success']) ?>
                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
