<?php

/* @var $this \yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $model \backend\modules\user\forms\Login */
$this->title = Yii::t('app', 'Авторизация');
$form = ActiveForm::begin([
    'fieldConfig' => [
        'template' => "{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    ],
    'options' => [
        'class' => 'm-t'
    ]
]);
echo $form
    ->field($model, 'login')
    ->input('email', [
        'placeholder' => $model->getAttributeLabel('login')
    ]);
echo $form
    ->field($model, 'password')
    ->input('password', [
        'placeholder' => $model->getAttributeLabel('password')
    ]);
echo $form->field($model, 'remember')->checkbox([
    'class' => 'i-checks'
]);
echo Html::submitButton(Yii::t("app", "Войти"), [
    'class' => 'btn btn-primary block full-width m-b'
]);
?>
    <div class="text-center">
        <a href="<?= Url::to(['forgot']); ?>">
            <small><?= Yii::t('app', 'Забыли пароль?'); ?></small>
        </a>
        <p class="text-muted text-center">
            <small><?= Yii::t('app', 'У Вас нет аккаунта?'); ?></small>
        </p>
    </div>
<?php
echo Html::a('Зарегистрироваться', ['register'], [
    'class' => 'btn btn-sm btn-white btn-block'
]);
ActiveForm::end();