<?php

/* @var $this \yii\web\View */

/* @var $model \backend\modules\user\forms\Register */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Регистрация');
$success = Yii::$app->session->getFlash('success', false);
if ($success) {
    ?>
    <div class="alert alert-success">
        <p><?= Yii::t("app", "Запрос успешно выполнен"); ?></p>
        <p><?= Yii::t("app", "После модерации Вы получите email с инструкцией для входа"); ?></p>
    </div>
    <p class="text-center">
        <a href="<?= Url::to(['login']); ?>">
            <small><?= Yii::t('app', 'Авторизация'); ?></small>
        </a>
    </p>
    <?php
} else {
    $form = ActiveForm::begin([
        'fieldConfig' => [
            'template' => "{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        ],
        'options' => [
            'class' => 'm-t'
        ]
    ]);
    echo $form->field($model, 'login')->input('email', [
        'placeholder' => $model->getAttributeLabel('login')
    ]);
    echo $form->field($model, 'password')->input('text', [
        'placeholder' => $model->getAttributeLabel('password')
    ]);
    echo $form->field($model, 'name')->input('text', [
        'placeholder' => $model->getAttributeLabel('name')
    ]);
    echo Html::submitButton(Yii::t("app", "Регистрация"), [
        'class' => 'btn btn-primary block full-width m-b'
    ]);
    ?>
    <p class="text-center">
        <a href="<?= Url::to(['login']); ?>">
            <small><?= Yii::t('app', 'Авторизация'); ?></small>
        </a>
    </p>
    <?php
    ActiveForm::end();
}