<?php

/* @var $this \yii\web\View */
/* @var $model \backend\modules\user\forms\ChangePassword */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Смена пароля');

$form = ActiveForm::begin([
    'fieldConfig' => [
        'template' => "{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    ],
    'options' => [
        'class' => 'm-t'
    ]
]);

echo $form->field($model, 'password')->input('text', [
    'placeholder' => $model->getAttributeLabel('password')
]);
echo Html::submitButton(Yii::t("app", "Сменить пароль"), [
    'class' => 'btn btn-primary block full-width m-b'
]);
ActiveForm::end();
