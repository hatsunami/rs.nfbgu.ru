<?php

/* @var $this \yii\web\View */
/* @var $model \backend\modules\user\forms\Forgot */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Восстановление пароля');
$success = Yii::$app->session->getFlash('success', false);
if ($success) {
    ?>
    <div class="alert alert-success text-center">
        <p><?= Yii::t("app", "Запрос успешно выполнен"); ?></p>
        <p><?= Yii::t("app", "На указанный Вами email была отправлена инструкция по восстановлению пароля"); ?></p>
    </div>
    <p class="text-center">
        <a href="<?= Url::to(['login']); ?>">
            <small><?= Yii::t('app', 'Авторизация'); ?></small>
        </a>
    </p>
<?php
} else {
    $form = ActiveForm::begin([
        'fieldConfig' => [
            'template' => "{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        ],
        'options' => [
            'class' => 'm-t'
        ]
    ]);
    echo $form->field($model, 'login')->input('email', [
        'placeholder' => $model->getAttributeLabel('login')
    ]);
    echo Html::submitButton(Yii::t("app", "Восстановить"), [
        'class' => 'btn btn-primary block full-width m-b'
    ]);
    ?>
    <p class="text-center">
        <a href="<?= Url::to(['login']); ?>">
            <small><?= Yii::t('app', 'Авторизация'); ?></small>
        </a>
    </p>
    <?php
    ActiveForm::end();
}