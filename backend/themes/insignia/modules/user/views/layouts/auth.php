<?php

use yii\helpers\Html;
use backend\themes\insignia\assets\Insignia;

$asset = Insignia::register($this);

$this->beginPage();
?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="<?= Yii::$app->charset; ?>">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <?= Html::csrfMetaTags(); ?>
        <title><?= $this->title; ?></title>
        <?php
        $this->head();
        ?>
    </head>
    <body class="gray-bg">
    <?php $this->beginBody(); ?>
    <div class="loginFlex middle-box loginscreen animated fadeInDown">
        <div>
            <div class="m-ta text-center">
                <h2>НФ БашГУ</h2>
            </div>
            <h3 class="text-center"><?= $this->title; ?></h3>
            <?= $content; ?>
            <p class="m-t text-center">
                <small><?= Yii::t('app', 'НФ БашГУ'); ?> &copy; <?= date('Y'); ?></small>
            </p>
        </div>
    </div>

    <?php $this->endBody(); ?>
    </body>
    </html>
<?php
$this->endPage();