<?php

namespace backend\modules\item;


class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\item\controllers';
}