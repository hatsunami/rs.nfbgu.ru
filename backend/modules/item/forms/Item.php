<?php

namespace backend\modules\item\forms;

use common\models\db\ItemAuthor;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class Item extends Model
{
    public $isNewRecord = true;
    public $id;
    public $title;
    public $type;
    public $source_id;
    public $first_page;
    public $last_page;
    public $volume;
    public $year;
    public $half_year;

    public $user;
    public $authors;

    public function rules()
    {
        return [
            [['title', 'type', 'source_id', 'first_page', 'last_page', 'volume', 'year', 'half_year','authors'], 'required'],
            [['type', 'source_id', 'first_page', 'last_page', 'year', 'half_year'], 'integer'],
            [['volume'], 'number'],
            [['title'], 'string', 'max' => 255],
            [['authors'], 'each', 'rule' => ['integer']],
        ];
    }

    public function attributeLabels()
    {
        return array_merge(
            (new \common\models\db\Item())->attributeLabels(), [
                'source_id' => Yii::t('app', 'Источник'),
                'authors' => Yii::t('app', 'Авторы'),
            ]
        );
    }

    public function prepare(\common\models\db\Item $item){
        $this->setAttributes($item->getAttributes());
        $this->id = $item->id;
        $this->authors = array_values(
            ArrayHelper::map(
                ItemAuthor::find()->where([
                    "item" => $item->id
                ])->all(),
                "id",
                "author"
            )
        );
    }

    public function save()
    {
        if($this->validate()){
            $item = null;
            if(!$this->isNewRecord && !empty($this->id)){
                $item = \common\models\db\Item::findOne($this->id);
            }
            if(empty($item)){
                $item = new \common\models\db\Item();
            }
            $item->title = $this->title;
            $item->type = $this->type;
            $item->source_id = $this->source_id;
            $item->first_page = $this->first_page;
            $item->last_page = $this->last_page;
            $item->user = $this->user;
            if($item->first_page && $item->last_page && empty($this->volume)){
                $item->volume = round(((abs($item->last_page-$item->first_page) + 1) / 16), 3);
            } else {
                $item->volume = $this->volume;
            }
            $item->year = $this->year;
            $item->half_year = $this->half_year;
            $item->save();
            if($item->hasErrors()){
                $this->addErrors($item->getFirstErrors());
            }else{
                $flagError = false;
                foreach ($this->authors as $author) {
                    if ($this->isNewRecord || ItemAuthor::find()->where(["item" => $item->id, "author" => $author])->count('id') ==0){
                        $a = new ItemAuthor([
                            "item" => $item->id,
                            "author" => $author
                        ]);
                        $a->save();
                        if ($a->hasErrors()){
                            $flagError = true;
                            $this->addErrors($a->getFirstErrors());
                            break;
                        }
                    }
                }
                if ($flagError){
                    if ($this->isNewRecord){
                        ItemAuthor::deleteAll([
                            "item" => $item->id
                        ]);
                        $item->delete();
                    }
                }else{
                    if (!$this->isNewRecord){
                        ItemAuthor::deleteAll([
                            "AND",
                            ["item" => $item->id],
                            ["NOT IN", "author", $this->authors]
                        ]);
                    }
                }
            }
        }
        return !$this->hasErrors();
    }
}