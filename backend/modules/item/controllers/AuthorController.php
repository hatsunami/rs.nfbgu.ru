<?php

namespace backend\modules\item\controllers;

use Yii;
use common\models\db\Author;
use common\models\data\item\searches\AuthorSearch;
use backend\components\web\Controller;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * AuthorController implements the CRUD actions for Author model.
 */
class AuthorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Author models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AuthorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Author model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if(Yii::$app->request->isAjax){
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Author model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        if (Yii::$app->request->isAjax){
            $this->layout = false;
        }
        $model = new Author([
            'user' => Yii::$app->user->id
        ]);

        if ($model->load(REQUEST()->post()) && $model->save()) {
            if (REQUEST()->isAjax){
                RESPONSE()->format = Response::FORMAT_JSON;
                RESPONSE()->statusCode = 201;
                return [
                    "id" => $model->id,
                    "name" => $model->name
                ];
            }else{
                return $this->redirect(['index']);
            }
        }
        if ($model->hasErrors() && REQUEST()->isAjax){
            throw new BadRequestHttpException(current($model->getFirstErrors()));
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Author model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $author = $this->findModel($id);

        if($author->user != Yii::$app->user->id){
            throw new NotAcceptableHttpException(Yii::t('error', 'У Вас нет прав доступа на редактирование данного автора'));
        }

        $model = new Author();
        $model->isNewRecord = false;
        $model->user = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Author model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id = null)
    {
        if(empty($id)){
            $id = Yii::$app->request->getBodyParam('id');
        }
        if(!empty($id)){
            if(!is_array($id)){
                $id = [$id];
            }
            $authors = Author::find()->where(['in', 'id', $id])->all();
            if(!empty($authors)){
                foreach ($authors as $author) {
                    if($author->user != Yii::$app->user->id){
                        if ($isArray) {
                            continue;
                        }else {
                            throw new NotAcceptableHttpException(Yii::t('error', 'У Вас нет прав доступа на удаление данного автора'));
                        }
                    }
                    $author->delete();
                }
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Author model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Author the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Author::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'Данной записи не существует.'));
    }
}
