<?php

namespace backend\modules\item\controllers;

use Yii;
use common\models\db\Source;
use common\models\data\item\searches\SourceSearch;
use backend\components\web\Controller;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * SourceController implements the CRUD actions for Source model.
 */
class SourceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Source models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SourceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Source model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if(Yii::$app->request->isAjax){
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Source model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (REQUEST()->isAjax){
            $this->layout = false;
        }
        $model = new Source([
            'user' => Yii::$app->user->id
        ]);

        if ($model->load(REQUEST()->post()) && $model->save()) {
            if (REQUEST()->isAjax){
                RESPONSE()->format = Response::FORMAT_JSON;
                RESPONSE()->statusCode = 201;
                return [
                    "id" => $model->id,
                    "name" => $model->name
                ];
            }else{
                return $this->redirect(['index']);
            }
        }
        if ($model->hasErrors() && REQUEST()->isAjax){
            throw new BadRequestHttpException(current($model->getFirstErrors()));
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Source model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $source = $this->findModel($id);

        if($source->user != Yii::$app->user->id){
            throw new NotAcceptableHttpException(Yii::t('error', 'У Вас нет прав доступа на редактирование данного источника'));
        }

        $model = new Source();
        $model->isNewRecord = false;
        $model->user = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Source model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id = null)
    {
        if(empty($id)){
            $id = Yii::$app->request->getBodyParam('id');
        }
        if(!empty($id)){
            if(!is_array($id)){
                $id = [$id];
            }
            $sources = Source::find()->where(['in', 'id', $id])->all();
            if(!empty($sources)){
                foreach ($sources as $source) {
                    if($source->user != Yii::$app->user->id){
                        if ($isArray) {
                            continue;
                        }else {
                            throw new NotAcceptableHttpException(Yii::t('error', 'У Вас нет прав доступа на удаление данного источника'));
                        }
                    }
                    $source->delete();
                }
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Source model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Source the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Source::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
