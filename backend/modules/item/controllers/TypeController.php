<?php

namespace backend\modules\item\controllers;

use Yii;
use common\models\db\Type;
use common\models\data\item\searches\TypeSearch;
use backend\components\web\Controller;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * TypeController implements the CRUD actions for Type model.
 */
class TypeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Type models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Type model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if(Yii::$app->request->isAjax){
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Type model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (REQUEST()->isAjax){
            $this->layout = false;
        }

        $model = new Type([
            'user' => Yii::$app->user->id
        ]);

        if ($model->load(REQUEST()->post()) && $model->save()) {
            if (REQUEST()->isAjax){
                RESPONSE()->format = Response::FORMAT_JSON;
                RESPONSE()->statusCode = 201;
                return [
                    "id" => $model->id,
                    "name" => $model->name
                ];
            }else{
                return $this->redirect(['index']);
            }
        }
        if ($model->hasErrors() && REQUEST()->isAjax){
            throw new BadRequestHttpException(current($model->getFirstErrors()));
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Type model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $type = $this->findModel($id);

        if($type->user != Yii::$app->user->id){
            throw new NotAcceptableHttpException(Yii::t('error', 'У Вас нет прав доступа на редактирование данного типа'));
        }

        $model = new Type();
        $model->isNewRecord = false;
        $model->user = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Type model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id = null)
    {
        if(empty($id)){
            $id = Yii::$app->request->getBodyParam('id');
        }
        if(!empty($id)){
            if(!is_array($id)){
                $id = [$id];
            }
            $types = Type::find()->where(['in', 'id', $id])->all();
            if(!empty($types)){
                foreach ($types as $type) {
                    if($type->user != Yii::$app->user->id){
                        if ($isArray) {
                            continue;
                        }else {
                            throw new NotAcceptableHttpException(Yii::t('error', 'У Вас нет прав доступа на удаление данного типа'));
                        }
                    }
                    $type->delete();
                }
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Type model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Type the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Type::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
