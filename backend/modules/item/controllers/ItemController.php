<?php

namespace backend\modules\item\controllers;

use Yii;
use common\models\db\Item;
use backend\modules\item\forms\Item as _Item;
use common\models\data\item\searches\ItemSearch;
use backend\components\web\Controller;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ItemController implements the CRUD actions for Item model.
 */
class ItemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return
            ArrayHelper::merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::class,
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ]
                ]
            );
    }

    /**
     * Lists all Item models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Item model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if(Yii::$app->request->isAjax){
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Item model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new _Item([
            'user' => Yii::$app->user->id
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isAjax){
                Yii::$app->response->statusCode = 201;
                Yii::$app->response->format = Response::FORMAT_JSON;
                return $model;
            }
            return $this->redirect(['index']);
        }
        if ($model->hasErrors() && Yii::$app->request->isAjax){
            throw new BadRequestHttpException(current($model->getFirstErrors()));
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Item model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $item = $this->findModel($id);
        if($item->user != Yii::$app->user->id){
            throw new NotAcceptableHttpException(Yii::t('error', 'У Вас нет прав доступа на редактирование данной статьи'));
        }
        $model = new _Item();
        $model->prepare($item);
        $model->isNewRecord = false;
        $model->user = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Item model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id = null)
    {
        if(empty($id)){
            $id = Yii::$app->request->getBodyParam('id');
        }
        if(!empty($id)){
            $isArray = is_array($id);
            if(!$isArray){
                $id = [$id];
            }
            $items = Item::find()->where(['in', 'id', $id])->all();
            if(!empty($items)){
                foreach ($items as $item) {
                    if($item->user != Yii::$app->user->id){
                        if ($isArray) {
                            continue;
                        }else {
                            throw new NotAcceptableHttpException(Yii::t('error', 'У Вас нет прав доступа на удаление данной статьи'));
                        }
                    }
                    $item->delete();
                }
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Item::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
