<?php

namespace backend\modules\user\controllers;

use backend\modules\user\forms\ChangeProfile;
use common\models\db\EmailStock;
use Yii;
use common\models\db\User;
use backend\modules\user\searches\ItemSearch;
use backend\components\web\Controller;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ItemController implements the CRUD actions for User model.
 */
class ItemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if(Yii::$app->request->isAjax){
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ChangeProfile();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        $model->password = null;

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $item = $this->findModel($id);

        $model = new ChangeProfile([
            'isNewRecord' => false,
        ]);
        $model->prepare($item);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($model->id == IDENTITY()->id) {
                $success = Yii::$app->session->setFlash('successChange', true);
            } else {
                return $this->redirect(['index']);
            }
        }
        $model->password = null;
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionProfile()
    {
        return $this->actionUpdate(IDENTITY()->id);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id = null)
    {
        if(empty($id)){
            $id = Yii::$app->request->getBodyParam('id');
        }
        if(!empty($id)){
            $isArray = is_array($id);
            if(!$isArray){
                $id = [$id];
            }
            $items = User::find()->where(['in', 'id', $id])->andWhere(['!=', 'id', IDENTITY()->id])->all();
            if(!empty($items)){
                foreach ($items as $item) {
                    $item->delete();
                }
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
