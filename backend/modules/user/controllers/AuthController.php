<?php

namespace backend\modules\user\controllers;

use backend\modules\user\forms\ChangePassword;
use backend\modules\user\forms\Register;
use backend\modules\user\forms\Restore;
use Yii;
use backend\components\web\Controller;
use backend\modules\user\forms\Login;
use backend\modules\user\forms\Forgot;
use yii\web\BadRequestHttpException;

class AuthController extends Controller
{
    public $layout = 'auth';
    public function actionLogin() {
        $model = new Login();

        if($model->load(Yii::$app->request->getBodyParams()) && $model->validate()) {
            $model->login();
            return $this->redirect(Yii::$app->homeUrl);
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->redirect(Yii::$app->user->loginUrl);
    }

    public function actionForgot() {
        $model = new Forgot();

        if($model->load(Yii::$app->request->getBodyParams()) && $model->forgot()) {
            Yii::$app->session->setFlash('success', true, true);
        }
        return $this->render('forgot', [
            'model' => $model,
        ]);
    }

    public function actionRegister() {
        $model = new Register();

        if($model->load(Yii::$app->request->getBodyParams()) && $model->register()) {
            Yii::$app->session->setFlash('success', true, true);
        }
        return $this->render('register', [
            'model' => $model,
        ]);
    }

    public function actionRestore() {
        $restore = new Restore();

        if(!$restore->restore(Yii::$app->request->getQueryParams())){
            throw new BadRequestHttpException(!$restore->hasErrors() ? Yii::t('error', 'Ошибка данных') : current($restore->getFirstErrors()));
        }

        $model = new ChangePassword($restore->user);
        if($model->load(Yii::$app->request->getBodyParams()) && $model->change()){
            if($restore->user->state == STATE_INACTIVE){
                return $this->redirect(Yii::$app->user->loginUrl);
            } else {
                Yii::$app->user->login($restore->user,SESSION_SHORT);
                return $this->redirect(Yii::$app->homeUrl);
            }
        }
        return $this->render('restore', [
            'model' => $model,
        ]);
    }
}