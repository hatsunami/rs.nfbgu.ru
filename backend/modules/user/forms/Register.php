<?php
/**
 * Created by PhpStorm.
 * User: gilim
 * Date: 22.03.2018
 * Time: 15:36
 */

namespace backend\modules\user\forms;


use backend\models\BackendUser;
use common\models\db\EmailStock;
use common\models\db\User;
use common\models\db\UserData;
use Yii;
use yii\base\Model;

class Register extends Model
{
    public $login;
    public $password;
    public $name;

    public function rules()
    {
        return [
            [['login', 'password'], 'required'],
            [['login'], 'email'],
            [['password', 'name'], 'string'],
        ];
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        if (parent::validate($attributeNames, $clearErrors)) {
            $user = BackendUser::find()->where([
                'like','email', $this->login, false
            ])->one();
            if(!empty($user)) {
                $this->addError('login', Yii::t('error', 'Пользователь с таким email уже существует'));
            }
        }
        return !$this->hasErrors();
    }

    public function register()
    {
        if ($this->validate()) {
            $user = new User();
            $user->email = $this->login;
            $user->password = $this->password;
            $user->save();

            if ($user->hasErrors()) {
                $this->addErrors($user->getFirstErrors());
            } else {
                $data = $user->baseUserData;
                if (empty($data)) {
                    $data = new UserData();
                    $data->id = $user->id;
                }
                $data->name = $this->name;
                $data->save();
                if ($data->hasErrors()) {
                    $this->addErrors($data->getFirstErrors());
                    if ($data->isNewRecord) {
                        $user->delete();
                    }
                } else {
                    EmailStock::add(
                        $this->login,
                        Yii::t("mail", "Регистрация"),
                        EmailStock::compose(
                            "register", [
                                "name" => $this->name
                            ]
                        )
                    );
                }
            }
        }
        return !$this->hasErrors();
    }

    public function attributeLabels()
    {
        return [
            'login' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Пароль'),
            'name' => Yii::t('app', 'ФИО'),
        ];
    }
}