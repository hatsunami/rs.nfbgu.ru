<?php
/**
 * Created by PhpStorm.
 * User: gilim
 * Date: 22.03.2018
 * Time: 15:36
 */

namespace backend\modules\user\forms;


use backend\models\BackendUser;
use common\models\db\EmailStock;
use Yii;
use yii\base\Model;

class Forgot extends Model
{
    public $login;

    private $user;

    public function rules()
    {
        return [
            [['login'], 'required'],
            [['login'], 'email'],
        ];
    }

    public function formName() {
        return '';
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        if (parent::validate($attributeNames, $clearErrors)) {
            $user = BackendUser::find()->where([
                'like', 'email', $this->login, false
            ])->one();
            if (empty($user)) {
                $this->addError('login', Yii::t('error', 'Неверный логин или пароль'));
            } else {
                $this->user = $user;
            }
        }
        return !$this->hasErrors();
    }

    public function forgot()
    {
        if ($this->validate()) {
            $hash = Yii::$app->security->generateRandomString();
            $this->user->hash = $hash;
            $this->user->save();
            if ($this->user->hasErrors()) {
                $this->addErrors($this->user->getFirstErrors());
            } else {
                EmailStock::add(
                    $this->login,
                    Yii::t("mail", "Восстановление пароля"),
                    EmailStock::compose(
                        "restore", [
                            "hash" => $hash
                        ]
                    )
                );
            }
        }
        return !$this->hasErrors();
    }

    public function attributeLabels()
    {
        return [
            'login' => Yii::t('app', 'Email'),
        ];
    }
}