<?php
/**
 * Created by PhpStorm.
 * User: gilim
 * Date: 22.03.2018
 * Time: 15:36
 */

namespace backend\modules\user\forms;


use backend\models\BackendUser;
use Yii;
use yii\base\Model;
use yii\web\IdentityInterface;

class ChangePassword extends Model
{
    public $password;

    private $user;

    public function rules()
    {
        return [
            [['password'], 'required'],
            [['password'], 'string'],
        ];
    }

    public function __construct(IdentityInterface $user, array $config = [])
    {
        $this->user = $user;
        parent::__construct($config);
    }

    public function change()
    {
        if ($this->validate()) {
            $this->user->password = $this->password;
            $this->user->hash = null;
            $this->user->save();
            if ($this->user->hasErrors()) {
                $this->addErrors($this->user->getFirstErrors());
            }
        }
        return !$this->hasErrors();
    }

    public function attributeLabels()
    {
        return [
            'password' => Yii::t('app', 'Новый пароль'),
        ];
    }
}