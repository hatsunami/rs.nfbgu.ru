<?php

namespace backend\modules\user\forms;


use common\models\db\User;
use common\models\db\UserData;
use yii\base\Model;
use Yii;

class ChangeProfile extends Model
{
    public $email;
    public $name;
    public $password;
    public $type;
    public $state = STATE_ACTIVE;

    public $id;
    public $isNewRecord = true;

    private $_user;

    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['password', 'email'], 'required', 'when' => function(){
                return $this->isNewRecord;
            }, 'whenClient' => '
                function() {
                    return '. ($this->isNewRecord ? 'true' : 'false') .';
                }
            '],
            [['email'], 'email'],
            [['type', 'state', 'id'], 'integer'],
            [['password', 'name'], 'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Email'),
            'name' => Yii::t('app', 'Имя'),
            'password' => Yii::t('app', 'Пароль'),
            'type' => Yii::t('app', 'Тип пользователя'),
            'state' => Yii::t('app', 'Состояние'),
        ];
    }

    public function prepare(User $user)
    {
        $this->_user = $user;
        $this->id = $user->id;
        $this->email = $user->email;
        $this->name = $user->baseUserData->name;
        $this->type = $user->type;
        $this->state = $user->state;
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        if (parent::validate($attributeNames, $clearErrors)) {
            $user = User::find()->where(['like', 'email', $this->email, false]);
            if (!$this->isNewRecord) {
                $user->andWhere(['!=', 'id', $this->id]);
            }
            $user = $user->one();
            if (!empty($user)) {
                $this->addError('email', Yii::t('error', 'Пользователь с таким email уже существует'));
            }
        }
        return !$this->hasErrors();
    }

    public function save()
    {
        if ($this->validate()) {
            $user = &$this->_user;
            if (empty($user) && !empty($this->id)) {
                $user = User::findOne($this->id);
            }
            if (empty($user)) {
                $this->isNewRecord = true;
                $user = new User([
                    'email' => $this->email,
                ]);
            }
            if (!empty($this->password)) {
                $user->password = $this->password;
            }
            if(IDENTITY()->type == USER_TYPE_ADMIN){
                $user->type = $this->type;
            }
            $this->state = $this->isNewRecord ? $this->state : STATE_ACTIVE;
            $user->state = $this->state;
            $user->save();
            if ($user->hasErrors()) {
                $this->addErrors($user->getFirstErrors());
            } else {
                $userData = UserData::findOne($user->id);
                if (empty($userData)) {
                    $userData = new UserData([
                        'id' => $user->id,
                    ]);
                }
                $userData->name = $this->name;
                $userData->save();

                if ($userData->hasErrors()) {
                    $this->addErrors($userData->getFirstErrors());
                    if ($user->isNewRecord) {
                        $userData->delete();
                        $user->delete();
                    }
                }
            }
        }
        return !$this->hasErrors();
    }
}