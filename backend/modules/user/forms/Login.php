<?php
/**
 * Created by PhpStorm.
 * User: gilim
 * Date: 22.03.2018
 * Time: 15:36
 */

namespace backend\modules\user\forms;


use backend\models\BackendUser;
use common\models\db\User;
use Yii;
use yii\base\Model;

class Login extends \common\components\forms\Login
{
    public $remember;

    public function __construct(array $config = [])
    {
        parent::__construct(new BackendUser(), $config);
    }

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [
            ["remember"], "boolean"
        ];

        return $rules;
    }

    public function login() {
        Yii::$app->user->login($this->user, $this->remember ? SESSION_LONG : SESSION_SHORT);
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels["remember"] = Yii::t('app', 'Запомнить меня');
        return $labels;
    }
}