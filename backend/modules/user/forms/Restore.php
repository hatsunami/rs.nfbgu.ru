<?php
/**
 * Created by PhpStorm.
 * User: gilim
 * Date: 22.03.2018
 * Time: 15:36
 */

namespace backend\modules\user\forms;


use backend\models\BackendUser;
use Yii;
use yii\base\Model;

class Restore extends Model
{
    public $hash;

    public $user;

    public function rules()
    {
        return [
            [['hash'], 'required'],
            [['hash'], 'string'],
        ];
    }
    public function formName() {
        return '';
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        if (parent::validate($attributeNames, $clearErrors)) {
            $user = BackendUser::find()->where([
                'hash' => $this->hash
            ])->one();
            if (empty($user)) {
                $this->addError('hash', Yii::t('error', 'Невалидный хэш'));
            } else {
                $this->user = $user;
            }
        }
        return !$this->hasErrors();
    }

    public function restore($data)
    {
        return $this->load($data, '') && $this->validate();
    }

    public function attributeLabels()
    {
        return [
            'hash' => Yii::t('app', 'Хэш'),
        ];
    }
}