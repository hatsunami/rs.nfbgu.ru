<?php
/**
 * Created by PhpStorm.
 * User: gilim
 * Date: 22.03.2018
 * Time: 15:00
 */

namespace backend\modules\user;


class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\user\controllers';
}