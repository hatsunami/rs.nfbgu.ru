<?php

namespace backend\modules\user\searches;

use common\models\db\UserData;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\BackendUser;
/**
 * ItemSearch represents the model behind the search form of `common\models\db\User`.
 */
class ItemSearch extends BackendUser
{
    public $query;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['query'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
            ->joinWith('baseUserData');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'email' => [
                        'asc' => [
                            self::tableName() . ".email" => SORT_ASC
                        ],
                        'desc' => [
                            self::tableName() . ".email" => SORT_DESC
                        ],
                    ],
                    'name' => [
                        'asc' => [
                            self::tableName() . ".name" => SORT_ASC
                        ],
                        'desc' => [
                            self::tableName() . ".name" => SORT_DESC
                        ],
                    ],
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions

        $query
            ->orFilterWhere(['like', self::tableName() . '.email', $this->query])
            ->orFilterWhere(['like', UserData::tableName() . '.name', $this->query]);

        return $dataProvider;
    }
}
