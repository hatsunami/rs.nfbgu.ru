<?php

namespace backend\components\widgets;


class Menu extends \yii\widgets\Menu
{
    protected function renderItem($item)
    {
        $item["template"] = '<a href="{url}">';
        if (isset($item["icon"])){
            $item["template"] .= '<i class="fa fa-' .$item["icon"]. '"></i>&nbsp;';
        }
        $item["template"] .= '<span class="nav-label">{label}</span>';
        if (!empty($item["inner"])){
            $item["template"] .= '<span class="fa arrow"></span>';
        }
        $item["template"] .= '</a>';
        return parent::renderItem($item);
    }
}