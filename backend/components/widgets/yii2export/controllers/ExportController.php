<?php

namespace backend\components\widgets\yii2export\controllers;


use yii\web\Controller;
use yii\helpers\Json;

class ExportController extends Controller
{
    public function actionExcel()
    {
        $data = $this->getData();
        $searchModel = $data['searchModel'];
        $dataProvider = $data['dataProvider'];
        $title = 'Отчет';
        $tableName = $data['tableName'];
        $fields = $this->getFieldsKeys($searchModel->exportFields());

        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setTitle($title ? $title : $tableName);
        $letter = 65;
        foreach ($fields as $one) {
            $objPHPExcel->getActiveSheet()->getColumnDimension(chr($letter))->setAutoSize(true);
            $letter++;
        }
        $letter = 65;
        foreach ($fields as $one) {
            $objPHPExcel->getActiveSheet()->setCellValue(chr($letter) . '1', $searchModel->getAttributeLabel($one));
            $objPHPExcel->getActiveSheet()->getStyle(chr($letter) . '1')->getAlignment()->setHorizontal(
                \PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $letter++;
        }
        $row = 2;
        $letter = 65;
        foreach ($dataProvider->getModels() as $model) {
            foreach ($searchModel->exportFields() as $one) {
                if (is_string($one)) {
                    $objPHPExcel->getActiveSheet()->setCellValue(chr($letter) . $row, $model[$one]);
                    $objPHPExcel->getActiveSheet()->getStyle(chr($letter) . $row)->getAlignment()->setHorizontal(
                        \PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                } else {
                    $objPHPExcel->getActiveSheet()->setCellValue(chr($letter) . $row, $one($model));
                    $objPHPExcel->getActiveSheet()->getStyle(chr($letter) . $row)->getAlignment()->setHorizontal(
                        \PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                }
                $letter++;
            }
            $letter = 65;
            $row++;
        }

        header('Content-Type: application/vnd.ms-excel');
        $filename = 'Отчет по научным работам профессорско-преподавательского состава и студентов.xls';
        header('Content-Disposition: attachment;filename=' . $filename);
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    public function actionWord()
    {
        $data = $this->getData();
        $searchModel = $data['searchModel'];
        $dataProvider = $data['dataProvider'];
        $title = 'Отчет по научным работам профессорско-преподавательского состава и студентов';
        $tableName = $data['tableName'];
        $fields = $this->getFieldsKeys($searchModel->exportFields());

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        $sectionStyle = $section->getSettings();
        $sectionStyle->setLandscape();
        $sectionStyle->setBorderTopColor('C0C0C0');
        $sectionStyle->setMarginTop(300);
        $sectionStyle->setMarginRight(300);
        $sectionStyle->setMarginBottom(300);
        $sectionStyle->setMarginLeft(300);
        $phpWord->addTitleStyle(1, ['name' => 'HelveticaNeueLT Std Med', 'size' => 16], ['align' => 'center']); //h
        $section->addTitle('<p style="font-size: 24px; text-align: center;">' . $title ? $title : $tableName . '</p>');

        $table = $section->addTable(
            [
                'name' => 'Tahoma',
                'align' => 'center',
                'cellMarginTop' => 30,
                'cellMarginRight' => 30,
                'cellMarginBottom' => 30,
                'cellMarginLeft' => 30,
            ]);
        $table->addRow(300, ['exactHeight' => true]);
        foreach ($fields as $one) {
            $table->addCell(1800, [
                'bgColor' => 'eeeeee',
                'valign' => 'center',
                'borderTopSize' => 5,
                'borderRightSize' => 5,
                'borderBottomSize' => 5,
                'borderLeftSize' => 5
            ])->addText($searchModel->getAttributeLabel($one), ['bold' => true, 'size' => 12], ['align' => 'center']);
        }
        foreach ($dataProvider->getModels() as $model) {
            $table->addRow(300, ['exactHeight' => true]);
            foreach ($searchModel->exportFields() as $one) {
                if (is_string($one)) {
                    $table->addCell(1800, [
                        'valign' => 'center',
                        'borderTopSize' => 5,
                        'borderRightSize' => 5,
                        'borderBottomSize' => 5,
                        'borderLeftSize' => 5
                    ])->addText($model[$one], ['bold' => false, 'size' => 12], ['align' => 'left']);
                } else {
                    $table->addCell(1800, [
                        'valign' => 'center',
                        'borderTopSize' => 5,
                        'borderRightSize' => 5,
                        'borderBottomSize' => 5,
                        'borderLeftSize' => 5
                    ])->addText($one($model), ['bold' => false, 'size' => 12], ['align' => 'left']);
                }
            }
        }

        header('Content-Type: application/vnd.ms-word');
        $filename = 'Отчет по научным работам профессорско-преподавательского состава и студентов.docx';
        header('Content-Disposition: attachment;filename=' . $filename . ' ');
        header('Cache-Control: max-age=0');
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('php://output');
    }

    public function actionHtml()
    {
        ini_set('display_errors', 1);
        $data = $this->getData();
        $searchModel = $data['searchModel'];
        $dataProvider = $data['dataProvider'];
        $title = 'Отчет по научным работам профессорско-преподавательского состава и студентов';
        $tableName = $data['tableName'];
        $fields = $this->getFieldsKeys($searchModel->exportFields());

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $properties = $phpWord->getDocInfo();
        $properties->setTitle($title);

        $section = $phpWord->addSection();
        $section->addTitle($title ? $title : $tableName);
        $table = $section->addTable(
            [
                'name' => 'Tahoma',
                'size' => 12,
                'align' => 'center',
            ]);
        $table->addRow(300, ['exactHeight' => true]);
        foreach ($fields as $one) {
            $table->addCell(1800, [
                'bgColor' => 'eeeeee',
                'valign' => 'center',
                'borderTopSize' => 5,
                'borderRightSize' => 5,
                'borderBottomSize' => 5,
                'borderLeftSize' => 5
            ])->addText($searchModel->getAttributeLabel($one), ['bold' => true, 'size' => 10], ['align' => 'center']);
        }
        foreach ($dataProvider->getModels() as $model) {
            $table->addRow(300, ['exactHeight' => true]);
            foreach ($searchModel->exportFields() as $one) {
                if (is_string($one)) {
                    $table->addCell(1800, [
                        'valign' => 'center',
                        'borderTopSize' => 5,
                        'borderRightSize' => 5,
                        'borderBottomSize' => 5,
                        'borderLeftSize' => 5
                    ])->addText('<p style="margin-left: 10px;">' . $model[$one] . '</p>', ['bold' => false, 'size' => 10], ['align' => 'right']);
                } else {
                    $table->addCell(1800, [
                        'valign' => 'center',
                        'borderTopSize' => 5,
                        'borderRightSize' => 5,
                        'borderBottomSize' => 5,
                        'borderLeftSize' => 5
                    ])->addText('<p style="margin-left: 10px;">' . $one($model) . '</p>', ['bold' => false, 'size' => 10], ['align' => 'right']);
                }
            }
        }
        $filename = 'Отчет по научным работам профессорско-преподавательского состава и студентов.html';

        header('Content-Type: text/html');
        header('Content-Disposition: attachment;filename=' . $filename . ' ');
        header('Cache-Control: max-age=0');
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');
        $objWriter->save('php://output');
        exit;
    }

    private function getData()
    {
        $queryParams = Json::decode(\Yii::$app->request->post('queryParams'));
        $searchModel = \Yii::$app->request->post('model');
        $searchModel = new $searchModel;
        $tableName = $searchModel->tableName();
        $dataProvider = $searchModel->search($queryParams);
        $title = \Yii::$app->request->post('title');
        $getAll = \Yii::$app->request->post('getAll');
        if ($getAll) {
            $dataProvider->pagination = false;
        }
        return [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'title' => $title,
            'tableName' => $tableName
        ];
    }

    private function getFieldsKeys($fieldsSended)
    {
        $fields = [];
        $i = 0;
        foreach ($fieldsSended as $key => $value) {
            if (is_int($key)) {
                $fields[$i] = $value;
            } else {
                $fields[$i] = $key;
            }
            $i++;
        }
        return $fields;
    }


}