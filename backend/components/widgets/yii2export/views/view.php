<?php

use yii\bootstrap\Html;

$types = [];
if ($widget->xls) $types['xls'] = 'excel';
if ($widget->word) $types['word'] = 'word';
if ($widget->html) $types['html'] = 'html';
if (!empty($types)) {
    ?>
    <div class="btn-group">
        <?= Html::a('<i class="fa fa-filter"></i> ' .Yii::t("app", "Фильтр"),
            'javascript:void(null);',
            [
                "class" => "btn btn-primary",
                "data" => [
                    "toggle" => "modal",
                    "target" => "#filter-view",
                    "pjax" => "0"
                ],

            ]) ?>
        <button data-toggle="dropdown" class="btn btn-success dropdown-toggle">
            <span class="fa fa-cloud-download"></span>
            <?= Yii::t("app", "Экспорт"); ?> <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            <?php
            foreach ($types as $type => $path) {
                ?>
                <li>
                    <?php
                    echo Html::beginForm('/export/' . $path, 'post');
                    echo Html::hiddenInput('fileName', $widget->fileName);
                    echo Html::hiddenInput('model', $widget->model);
                    echo Html::hiddenInput('queryParams', $widget->queryParams);
                    echo Html::hiddenInput('getAll', $widget->getAll);
                    echo Html::hiddenInput('title', $widget->title);
                    $sb = $type . 'ButtonName';
                    switch ($type){
                        case 'xls':
                            $iconType = 'file-excel-o';
                            break;
                        case 'word':
                            $iconType ='file-word-o';
                            break;
                        case 'html':
                            $iconType = 'html5';
                            break;
                    }
                    echo Html::submitButton('<i class="fa fa-' . $iconType . '" aria-hidden="true"></i> ' . $widget->$sb,[
                        'class' => 'exportButton',
                        'format' => 'raw'
                    ]);

                    echo Html::endForm();
                    ?>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>

    <?php
}
