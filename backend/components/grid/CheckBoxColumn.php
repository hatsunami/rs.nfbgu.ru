<?php
/**
 * Created by PhpStorm.
 * User: gilim
 * Date: 27.03.2018
 * Time: 12:11
 */

namespace backend\components\grid;


use Closure;
use yii\helpers\Html;
use yii\helpers\Json;

class CheckBoxColumn extends \yii\grid\CheckboxColumn
{
    /**
     * Renders the header cell content.
     * The default implementation simply renders [[header]].
     * This method may be overridden to customize the rendering of the header cell.
     * @return string the rendering result
     */
    protected function renderHeaderCellContent()
    {
        $classes = [
            'class' => 'select-on-check-all i-checks'
        ];
        if ($this->header !== null || !$this->multiple) {
            return parent::renderHeaderCellContent();
        }

        return Html::checkbox($this->getHeaderCheckBoxName(), false, $classes);
    }

    /**
     * {@inheritdoc}
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        if ($this->checkboxOptions instanceof Closure) {
            $options = call_user_func($this->checkboxOptions, $model, $key, $index, $this);
        } else {
            $options = $this->checkboxOptions;
        }
        if (!empty($options['hidden'])){
            return false;
        }

        if (!isset($options['value'])) {
            $options['value'] = is_array($key) ? Json::encode($key) : $key;
        }

        if ($this->cssClass !== null) {
            Html::addCssClass($options, $this->cssClass);
        }
        if(!isset($options['class'])) {
            $options['class'] = 'i-checks';
        } else {
            if(is_array($options['class'])) {
                $options['class'][] = 'i-checks';
            } else {
                $options['class'] .= ' i-checks';
            }
        }
        return Html::checkbox($this->name, !empty($options['checked']), $options);
    }
}