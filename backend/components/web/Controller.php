<?php

namespace backend\components\web;


use yii\filters\AccessControl;
use Yii;

class Controller extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'denyCallback' => function() {
                    return $this->redirect(Yii::$app->user->loginUrl);
                },
                'rules' => [
                    [
                        'actions' => [
                            'error'
                        ],
                        'allow' => true,
                        'roles' => [
                            '?', '@'
                        ]
                    ],
                    [
                        'actions' => [
                            'login',
                            'register',
                            'forgot',
                            'restore'
                        ],
                        'allow' => true,
                        'roles' => [
                            '?'
                        ]
                    ],
                    [
                        'actions' => null,
                        'allow' => true,
                        'roles' => [
                            '@'
                        ]
                    ]
                ]
            ]
        ];
    }
}