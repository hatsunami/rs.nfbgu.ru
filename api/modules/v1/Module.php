<?php
namespace api\modules\v1;


class Module extends \yii\base\Module
{
    public $controllerNamespace = 'api\modules\v1\controllers';

    public function init()
    {
        parent::init();
        $this->modules =[
            "user" => [
                'class' => 'api\modules\v1\modules\user\Module'
            ],
            "item" => [
                'class' => 'api\modules\v1\modules\item\Module'
            ]
        ];
    }

}