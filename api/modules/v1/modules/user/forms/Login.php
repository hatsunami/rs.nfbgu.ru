<?php
namespace api\modules\v1\modules\user\forms;

use api\models\ApiUser;

class Login extends \common\components\forms\Login
{
    public function __construct(array $config = [])
    {
        parent::__construct(new ApiUser(), $config);
    }

    public function login(){
        return $this->user->token;
    }

    public function formName()
    {
        return "";
    }
}