<?php
namespace api\modules\v1\modules\user\controllers;

use api\components\web\Controller;
use yii\helpers\ArrayHelper;
use api\modules\v1\modules\user\forms\Login;
use yii\web\NotAcceptableHttpException;

class AuthController extends Controller
{
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                "authenticator" => [
                    "except" => [
                        "login"
                    ]
                ]
            ]
        );
    }

    public function actionLogin(){
        $model = new Login();

        if($model->load(REQUEST()->getBodyParams()) && $model->validate()) {
            $token = $model->login();
            return [
                "token" => $token
            ];
        }
        throw new NotAcceptableHttpException($model->hasErrors() ? current($model->getFirstErrors()) : null);
    }
}