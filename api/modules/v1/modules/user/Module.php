<?php
namespace api\modules\v1\modules\user;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'api\modules\v1\modules\user\controllers';
}