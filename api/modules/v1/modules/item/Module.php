<?php
namespace api\modules\v1\modules\item;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'api\modules\v1\modules\item\controllers';
}