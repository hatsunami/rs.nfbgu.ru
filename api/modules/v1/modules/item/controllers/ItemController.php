<?php

namespace api\modules\v1\modules\item\controllers;

use Yii;
use common\models\db\Item;
use common\models\data\item\forms\Item as _Item;
use common\models\data\item\searches\ItemSearch;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ItemController implements the CRUD actions for Item model.
 */
class ItemController extends \api\components\web\Controller
{
    public function behaviors()
    {
        return
            ArrayHelper::merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['DELETE'],
                        ],
                    ]
                ]
            );
    }

    public function actionIndex()
    {
        $searchModel = new ItemSearch();
        $dataProvider = $searchModel->search(REQUEST()->queryParams, "");
        return $this->_render($dataProvider);
    }

    public function actionView($id, $code = null)
    {
        return $this->_renderOne($this->findModel($id), $code);
    }

    public function actionCreate()
    {
        $model = new _Item([
            'user' => IDENTITY()->id
        ]);

        if ($model->load(REQUEST()->bodyParams, "") && $model->save()) {
            return $this->actionView($model->id, 201);
        }
        if ($model->hasErrors()){
            throw new BadRequestHttpException(current($model->getFirstErrors()));
        }
    }

    public function actionUpdate($id)
    {
        $item = $this->findModel($id);
        $model = new _Item([
            "isNewRecord" => false,
            "user" => IDENTITY()->id
        ]);
        $model->prepare($item);

        if ($model->load(REQUEST()->bodyParams, "") && $model->save()) {
            return $this->actionView($model->id);
        }
        if ($model->hasErrors()){
            throw new BadRequestHttpException(current($model->getFirstErrors()));
        }
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $deleted = [];
        if($model->user == IDENTITY()->id || IDENTITY()->type == USER_TYPE_ADMIN) {
            if ($model->delete()) {
                $deleted[] = $model->id;
            }
            RESPONSE()->setStatusCode(202);
        } else {
            RESPONSE()->setStatusCode(204);
        }
        return $deleted;
    }

    protected function findModel($id)
    {
        if (($model = Item::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('error', 'Публикация не найдена'));
    }
}
