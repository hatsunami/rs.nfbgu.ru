<?php

namespace api\modules\v1\modules\item\controllers;

use Yii;
use common\models\db\Type;
use common\models\data\item\searches\TypeSearch;
use yii\web\BadRequestHttpException;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class TypeController extends \api\components\web\Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['DELETE'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new TypeSearch();
        $dataProvider = $searchModel->search(REQUEST()->queryParams, "");
        return $this->_render($dataProvider);
    }

    public function actionView($id, $code = null)
    {
        return $this->_renderOne($this->findModel($id), $code);
    }

    public function actionCreate()
    {
        $model = new Type([
            'user' => IDENTITY()->id
        ]);

        if ($model->load(REQUEST()->bodyParams, "") && $model->save()) {
            return $this->actionView($model->id, 201);
        }
        if ($model->hasErrors()){
            throw new BadRequestHttpException(current($model->getFirstErrors()));
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if($model->user == IDENTITY()->id || IDENTITY()->type == USER_TYPE_ADMIN) {
            if ($model->load(REQUEST()->bodyParams, "") && $model->save()) {
                return $this->actionView($model->id);
            }
            if ($model->hasErrors()){
                throw new BadRequestHttpException(current($model->getFirstErrors()));
            }
        } else {
            throw new NotAcceptableHttpException(Yii::t('error', 'Вам не доступно это действие'));
        }
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $deleted = [];
        if($model->user == IDENTITY()->id || IDENTITY()->type == USER_TYPE_ADMIN) {
            if ($model->delete()) {
                $deleted[] = $model->id;
            }
            RESPONSE()->setStatusCode(202);
        } else {
            RESPONSE()->setStatusCode(204);
        }
        return $deleted;
    }

    protected function findModel($id)
    {
        if (($model = Type::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('error', 'Тип не найден'));
    }
}