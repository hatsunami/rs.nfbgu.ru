<?php
namespace api\controllers;

use api\components\web\Controller;
use Yii;

class SystemController extends Controller
{
    public function actionOptions(){
        return "OK";
    }

    public function actionError(){
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            throw new \HttpException($exception->getMessage(), $exception->getCode());
        }
    }
}