<?php
use yii\helpers\ArrayHelper;
use yii\web\Response;

$_config = [
    'id' => 'RS-api',
    'name' => 'RS API',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
                'application/xml' => Response::FORMAT_XML,
            ]
        ],
    ],
    'controllerNamespace' => 'api\controllers',
    'components' => [
        'errorHandler' => [
            'errorAction' => 'system/error',
        ],
        'user' => [
            'identityClass' => 'api\models\ApiUser',
            'enableAutoLogin' => false,
            'enableSession' => false
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $code = $event->sender->getStatusCode();
                if ($code >= 200 && $code < 300){
                    $event->sender->setStatusCode($code);
                }else{
                    $event->sender->setStatusCode(200);
                }
                $response = $event->sender;
                $body = $response->data;
                $data = [
                    "data" => $response->data,
                    "page" => isset($body["page"]) ? $body["page"] + 1 : 0,
                    "show" => isset($body["show"]) ? $body["show"] : 0,
                    "total" => isset($body["total"]) ? $body["total"] : 0,
                    "code" => $response->statusCode,
                    "updated_at" => time(),
                ];
                if ($response->statusCode >= 300){
                    $data["message"] = $response->data["message"];
                }
                array_map(function($ix) use (&$body){
                    if (isset($body[$ix])) unset($body[$ix]);
                }, ["page", "show", "total"]);

                $data["data"] = $body;
                if ($response->data !== null) {
                    if (YII_DEBUG) {
                        $data['elapsed_time'] = Yii::getLogger()->getElapsedTime();
                        $sqls = Yii::getLogger()->getProfiling([
                            'yii\db\Command::query',
                            'yii\db\Command::execute'
                        ]);

                        $data["sql"] = array_map(function($sql){
                            return [
                                $sql['info'],
                                $sql['duration'],
                            ];
                        }, $sqls);
                    }
                }
                $response->data = $data;
            }
        ],
        'request' => [
            'enableCookieValidation' => false,
            'enableCsrfValidation' => false,
            'parsers' => [
                '' => 'yii\web\JsonParser',
                'application/json' => 'yii\web\JsonParser',
                'application/xml' => 'light\yii2\XmlParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'i18n' => [
            'translations' => [
                'api' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ]
            ]
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'rules' => [
                #Общие
                "OPTIONS <api:v\d+><path:.+>" => "system/options",
                "PUT <api:v\d+>/user/login" => "<api>/user/auth/login",

                #Публикации
                "GET <api:v\d+>/item/" => "<api>/item/item/index",
                "GET <api:v\d+>/item/<id:\d+>" => "<api>/item/item/view",
                "POST <api:v\d+>/item/" => "<api>/item/item/create",
                "PUT <api:v\d+>/item/<id:\d+>" => "<api>/item/item/update",
                "DELETE <api:v\d+>/item/<id:\d+>" => "<api>/item/item/delete",

                #Типы, источники, авторы публикаций
                "GET <api:v\d+>/item/<controller:type|source|author>" => "<api>/item/<controller>/index",
                "GET <api:v\d+>/item/<controller:type|source|author>/<id:\d+>" => "<api>/item/<controller>/view",
                "POST <api:v\d+>/item/<controller:type|source|author>" => "<api>/item/<controller>/create",
                "PUT <api:v\d+>/item/<controller:type|source|author>/<id:\d+>" => "<api>/item/<controller>/update",
                "DELETE <api:v\d+>/item/<controller:type|source|author>/<id:\d+>" => "<api>/item/<controller>/delete",

                "<controller:.+>/<action:.+>" => "<controller>/<action>"
            ]
        ]

    ],
    'modules' => [
        'v1' => [
            'class' => 'api\modules\v1\Module'
        ]
    ],
];

return ArrayHelper::merge(
    (require_once COMMON_DIR . "/config/main.php"),
    $_config
);