<?php

defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('YII_ENV') or define('YII_ENV', YII_DEBUG ? 'dev' : 'env');
ini_set('display_errors', YII_DEBUG);

require __DIR__ . '/../../basepath.php';
require COMMON_DIR . '/helpers/constant.php';

require APP_DIR . '/vendor/autoload.php';
require APP_DIR . '/vendor/yiisoft/yii2/Yii.php';
require COMMON_DIR . '/config/bootstrap.php';

$config = require API_DIR . '/config/main.php';
$app = new yii\web\Application($config);
require COMMON_DIR . '/helpers/shortcut.php';
$app->run();
