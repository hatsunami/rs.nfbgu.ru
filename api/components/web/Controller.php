<?php

namespace api\components\web;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\Cors;
use yii\rest\Serializer;

class Controller extends \yii\rest\Controller
{
    protected function _render(ActiveDataProvider $dataProvider)
    {
        $serializer = new Serializer();
        return [
            "items" => $serializer->serialize($dataProvider),
            "total" => $dataProvider->totalCount,
            "show" => $dataProvider->pagination->pageSize,
            "page" => $dataProvider->pagination->page
        ];
    }

    protected function _renderOne(Model $data, $code = null)
    {
        if (is_numeric($code)){
            RESPONSE()->statusCode = $code;
        }
        return $data;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        $behaviors["corsFilter"] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Allow-Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'OPTIONS', 'DELETE'],
                'Access-Control-Request-Headers' => ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'Authorization'],
                'Access-Control-Allow-Credentials' => true
            ]
        ];
        return $behaviors;
    }
}